<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jdsofttech');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1j(!Ey% ;S|w;5<D{7IUJ3e+YYuy0Kzg:f!UMR[.p64j:v)kuuiKEK^BY:;mJ6lE');
define('SECURE_AUTH_KEY',  '[Mv29Ouv[[wsub_uPRj1r%NIh+bv4<xJO8G9/tPbb9A5WJoAAz`Cp2s<?FV<GtnX');
define('LOGGED_IN_KEY',    '=b:J!5 -El6|&Ocl l4U)IrI1?d$2r2bQXmA NDnx1*1aQ$]Ot&^fh&*n:V#iLN]');
define('NONCE_KEY',        '^M@s%Mj(zgr5oybYeJ 08Aj[@:[I,eW4ypWNLG3FyA]4}NS4njCxB@QAa%)~gV<N');
define('AUTH_SALT',        'R~a:wcu<PMm}3]pv&m+0Ra,?j+EHaN:PLciapN@P$MtXNLD 7~/|r8=;%np`*piZ');
define('SECURE_AUTH_SALT', 't8!V,Q)$egc1Po~,~Rj(n5fd6y5U)9.0o`k*Pxf)SIJ4S//xRVmD%L?z;t^r~gr/');
define('LOGGED_IN_SALT',   's&A,;}k>?Hl7TA7-LpRPydt(Vf23ECm4o?ij0pN8OqK4,GLw0bT>)XrJ-GsR=F6R');
define('NONCE_SALT',       'NdD`RF]Q#-|u(ssr=V2N7G:!HCzvo#%XFTfnSk l-i9)jZz8AnHNw<9XXx *Tb1l');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

// ini_set( 'sendmail_from', "vpatel@taskme.biz" ); 
// ini_set( 'SMTP', "localhost" );  
// ini_set( 'smtp_port', 465 );