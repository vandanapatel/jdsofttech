<?php 

$emailid = 'submit_email_'.$post->ID;
if(isset($_POST[$emailid])){

    $invoice_id = $_POST['mail_invoice_id']; 

    $customer = get_field('cust_invoice_customer', $invoice_id);

    $customer_email = get_field('customer_email', $customer->ID);

    $custom_email = $_POST['custom_emailid'];

    if( $_POST['radio_invoice_email'] == 'radio_custom_email' ){
       $to = $_POST['custom_emailid'];

      

    }elseif( $_POST['radio_invoice_email'] == 'radio_customer_email'){

       $to = get_field('customer_email', $customer->ID);

    }

    // $to = "nbhalala.jd@gmail.com";
    $subject = "My subject";

    $invoice_no = get_field('cust_invoice_number', $invoice_id);
       
    if( have_rows('customer_address', $customer->ID) ): 
      while ( have_rows('customer_address', $customer->ID) ) : the_row(); 
        $street = get_sub_field('customer_street', $customer->ID); 
        $zipcode = get_sub_field('customer_zipcode', $customer->ID);
        $state = get_sub_field('customer_state', $customer->ID);
        $city = get_sub_field('customer_city', $customer->ID);

        $otherinfo = '<div class="customer" style="background: #e7e7e8;border: 8px solid #bcbdc0;padding: 78px 0px 70px 40px; max-height: 500px; position: relative;"> 
                        <span style="text-transform: uppercase;color: #818285;font-size: 20px;display: block;margin-bottom: 10px;"> Customer: </span> <a href="'. get_permalink($customer->ID) .'" style="color: #000; font-size: 30px;display: block; margin-bottom: 10px; text-decoration: none;outline: none;">'. get_field('customer_name', $customer->ID) .'</a>              
                        <p style="font-size: 24px; color: #333; margin-bottom: 10px; line-height: 40px; margin: 0;">'. $street .'<br>'. $zipcode . ' ' . $state . '<br>' .
                          'Ph : + '. get_field('customer_phone_no', $customer->ID).
                        '</p>
                      </div>';        
      endwhile;
    endif;


    global $website_content, $tbody, $tbodyclose, $total;            
    $website_content = '';   
    if( have_rows('cust_invoice_website', $invoice_id) ):  
    $tbody = '<tbody>';
          while ( have_rows('cust_invoice_website', $invoice_id) ) : the_row(); 
          
            $website_content .= '<tr style="background-color: rgba(0,0,0,.05);">
                <td class="col-description" style="border-right: 4px solid #fff; padding: 15px; line-height: 20px; text-align: left; vertical-align: top;color: #000; font-size: 18px;">'. get_sub_field('cust_invoice_desc', $invoice_id) .'</td>
                <td class="col-description" style="border-right: 4px solid #fff; padding: 15px; line-height: 20px; text-align: left; vertical-align: top;color: #000; font-size: 18px;">'. get_sub_field('cust_invoice_quantity', $invoice_id) .'</td>
                <td class="col-description" style="border-right: 4px solid #fff; padding: 15px; line-height: 20px; text-align: left; vertical-align: top;color: #000; font-size: 18px;">'. get_sub_field('cust_invoice', $invoice_id) .'</td>
                <td class="col-amount" style="padding: 15px; line-height: 20px; text-align: right; vertical-align: top; color: #2186c8; font-size: 18px;"> $'. get_sub_field('cust_invoice_amount', $invoice_id) .'</td>
              </tr>
              <tr style="background: #faf9fc;height: 30px;">             
                <td class="col-description" style="border-right: 4px solid #fff;"></td>                       
                <td class="col-description" style="border-right: 4px solid #fff;"></td>
                <td class="col-description" style="border-right: 4px solid #fff;"> </td>
                <td class="col-amount"></td>
              </tr>';
        $total += intval( get_sub_field('cust_invoice_amount', $invoice_id));
        endwhile; 
        $tbodyclose = '</tbody>
                      <tfoot>
                        <tr>
                          <td colspan="3" style="padding: 15px; line-height: 20px; text-align: left; vertical-align: top;"><div class="total" style="background-color: #2186c8; color: #fff; padding: 20px 0px; min-width: 150px; text-transform: uppercase; font-size: 20px; float: right; text-align: center;">Total</div></td>
                          <td style="padding: 15px; line-height: 20px; text-align: left; vertical-align: top;"><div class="total amount" style="width: 94%; border-left: 2px solid #fff;text-align: right; background-color: #2186c8; color: #fff; padding: 20px 15px; min-width: 150px; text-transform: uppercase; font-size: 20px; float: right;"> $'. $total .'</div></td>
                        </tr>
                      </tfoot>';
    endif;


    $invoice_date = get_field('cust_invoice_invdate', $invoice_id); 
    $due_date = get_field('cust_invoice_duedate', $invoice_id);                                               

    $txt = '<!DOCTYPE html>
          <html lang="en">
            <head>
              <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <style>
               h1.logo_cls{
                  color:#fff;
               }
               .customer_afimage{
                width:94px;
                height86px;
                border-right: 1px solid #d1d2d4; 
               }
              </style>          
            </head>
            <body>
            <table cellspacing="0">             
              <tr>
                <td>
                  <section class="content-section single_invoice_div" style="background-color: #ffffff;padding: 0px 70px;min-height: 600px; margin: 0 auto;width:1250px;">              
                    <div class="top-heading" style=" background: url(https://jdsofttech.com/invoice/wp-content/themes/jdsofttech/assets/img/ribbon.png) no-repeat center center;width: 619px; height: 75px;background-size: cover;text-align: center;margin:0 auto;">
                      <h1 class="logo_cls" style="margin: 0;color:#fff; line-height: 70px;font-size: 36px;"> Invoice number: '. $invoice_no .' </h1>
                    </div>
                    <div class="logo-block-row" style=" position: relative;text-align: center; margin: 10px 0px; background: url(https://jdsofttech.com/invoice/wp-content/themes/jdsofttech/assets/img/logo-block-row-line.png) no-repeat center;background-size: contain;"> 
                      <a class="logo" href="'.site_url() .'"><img src="https://jdsofttech.com/invoice/wp-content/themes/jdsofttech/assets/img/logo.png" alt="JDsofttech"/ style=" display: inline-block;max-width: 300px;position: relative;z-index: 2;"> </a> 
                    </div>
                    <div class="supplier-customer-box-row">
                      <div class="table-responsive-md">
                          <table width="0" border="0" cellpadding="0" cellspacing="0" style="width: 100%;border-top: 1px solid #d1d2d4;margin-bottom: 50px; margin-top: 45px;">
                            <tbody>
                              <tr>
                                <td class="left-colum" style="width: 50%; padding: 0px 0px 0px 15px; vertical-align: top; border-right: 1px solid #d1d2d4;">

                                  <table cellspacing="0" cellpadding="-8" width="101.5%">
                                    <tr><td style="text-align: right; vertical-align: bottom;"><img src="https://jdsofttech.com/invoice/wp-content/themes/jdsofttech/assets/img/customer-ribon.png)" align="right" style="display:block;text-align: right; vertical-align: top; "></td></tr>
                                    <tr>
                                      <td>
                                        <div class="supplier" style="padding: 0px 0px 15px 0px;"> <span style="text-transform: uppercase;color: #818285;font-size: 20px;display: block;margin-bottom: 10px;"> supplier </span> <a href="'. site_url() .'" style="color: #2185c7; font-size: 30px; display: block; text-transform: uppercase; margin-bottom: 10px; text-decoration: none; outline: none;"> JD Softtech INC . </a>
                                          <p style="font-size: 24px;color: #333;margin-bottom: 10px;line-height: 40px;"> Web & Software Developments Company </p>
                                        </div>
                                        <div class="address" style="padding: 15px 0px 15px 0px; margin-bottom: 70px;"> <span style="text-transform: uppercase;color: #818285;font-size: 20px;display: block;margin-bottom: 10px;"> Address </span>
                                          <p style="font-size: 24px;color: #333;margin-bottom: 10px;line-height: 40px;"> 903 Main Street, Hanson,<br>
                                            02341,MA,<br>
                                            USA </p>
                                          <ul style="list-style: none; padding: 0;">
                                            <li style="display: block;margin-left:0;"> <span style="text-transform: uppercase;color: #818285;font-size: 20px;display: inline-block;margin-bottom: 10px;"> Phone: </span> <a href="tel:+1 (781) 9744568" style="font-size: 24px;color: #2186c8; text-decoration: none; outline: none; display: inline-block;">
                                              <p style="font-size: 24px;color: #333;margin-bottom: 10px;line-height: 40px;">+1 (781) 9744568 </p>
                                              </a> </li>
                                            <li style="display: block;margin-left:0;"> <span style="text-transform: uppercase;color: #818285;font-size: 20px;display: inline-block;margin-bottom: 10px;"> Website: </span> <a href="https://www.jdsofttech.com/" style=" font-size: 24px; color: #2186c8;"> www.jdsofttech.com </a> </li>
                                            <li style="display: block;margin-left:0;"> <span style="text-transform: uppercase;color: #818285;font-size: 20px;display: inline-block;margin-bottom: 10px;"> E-mail: </span> <a href="mailto:info@jdsofttech.com" style=" font-size: 24px; color: #2186c8;"> info@jdsofttech.com </a> </li>
                                          </ul>
                                        </div>


                                        </td>
                                        </tr>
                                  </table>


                                </td>

                                <td class="right-culum customerinfo" style="width: 50%; vertical-align: top; padding: 0px;">
                                  '.$otherinfo . '                              
                                </td>
                              </tr>
                            </tbody>
                            <tfoot>
                              <tr>                           
                                <td class="left-colum foot-left-col" style="background: #2186c8; padding: 15px 30px; font-size: 30px; color: #fff; border-right: 1px solid #ffffff; border-bottom: 0;"> Date : '. $invoice_date .'</td>
                                <td class="right-colum foot-right-col" style="background: #e7e7e8; padding: 15px 30px; font-size: 30px; color: #818285; text-align: right; border-bottom: 0;"> Date : '. $due_date .'</td>
                              </tr>
                            </tfoot>
                          </table>
                      </div>
                    </div>
                    <div class="description-amount-table-row">
                      <div class="table-responsive-sm">
                        <table class="table-striped" style="margin-bottom: 50px;width: 100%; border-collapse: collapse;">
                          <thead style="background-color: #231f20;color: #fff; text-transform: uppercase; font-size: 20px; font-weight: normal;">
                              <tr>
                                <th class="col-description" style="border-right: 4px solid #fff; padding: 15px; line-height: 20px; text-align: left; vertical-align: top;"> Description </th>
                                <th class="col-description" style="border-right: 4px solid #fff; padding: 15px; line-height: 20px; text-align: left; vertical-align: top;"> Quantity </th>
                                <th class="col-description" style="border-right: 4px solid #fff; padding: 15px; line-height: 20px; text-align: left; vertical-align: top;"> Rate </th>
                                <th class="col-amount" style="padding: 15px; line-height: 20px; text-align: right; vertical-align: top;"> Amount </th>
                              </tr>
                          </thead>'. $tbody . $website_content . $tbodyclose .
                        '</table>
                      </div>
                    </div>
                    <div class="contenteditable" style=" padding: 50px 0px;">
                      <p style="font-size: 20px; color: #818285; margin: 0px;line-height: 30px;"> Amount in words: '. convertNum(  $total ) .' Dolloars Only. </p>
                      <p style="font-size: 20px; color: #818285; margin: 0px;line-height: 30px;"> Declaration: We declare that this invoice shows the actual price for LeadersUp and all particulars are true and  correct. </p>
                    </div>
                    <div class="thank-you-text" style="padding: 25px 0px; text-align: center; font-size: 30px; color: #818285; border-top: 1px solid #d1d2d4;"> Thank you for your business! </div>
                  </section>
                </td>
                </tr>
            </table>
          </body></html>';

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html; charset=iso-8859-1" . "\r\n";
    // $headers .= "Content-type: application/json" . "\r\n";
    // $headers .= "From: nbhalala.jd@gmail.com";
    $headers .= "From: vpatel@taskme.biz";

    wp_mail($to,$subject,$txt,$headers);

}
?>