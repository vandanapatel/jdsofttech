<?php 
// Template Name:Reset Password


if( is_user_logged_in() ) {
	wp_redirect(home_url());
 	exit();
}	

ob_start();
error_reporting(E_ALL);
ini_set("display_errors", 1);

get_header();

 //you can change the number 7 to whatever length needed for the new password

global $wpdb,$new_password;


if(isset($_GET['key']) && $_GET['action'] == "reset_pwd") {

	$reset_key = $_GET['key'];
	$user_login = $_GET['login'];
	$user_data = $wpdb->get_row($wpdb->prepare("SELECT ID, user_login, user_email FROM $wpdb->users WHERE user_activation_key = %s AND user_login = %s", $reset_key, $user_login));

// $user_login = $user_data->user_login;
// $user_email = $user_data->user_email;

	if(!empty($reset_key) && !empty($user_data)) {

		$new_password = wp_generate_password(7, false);
		
		if(isset($_POST['reset_pass'])){

			$new_pass = $_POST['user_pass'];
			wp_set_password( $new_pass, $user_data->ID );
			$redirect_to = get_bloginfo('url')."/log-in?action=reset_success";
			wp_safe_redirect($redirect_to);

		}
	} else { 
		exit(); 
	}
	
}


?>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/bg-01.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form resetpassform" id="resetpassform"  method="post" >
					<input type="hidden" id="user_login" value="<?php if(isset($_GET['key']) && $_GET['action'] == "reset_pwd") { echo $user_login; } else { ""; } ?>" autocomplete="off">
					<?php 

						$custom_logo_id = get_theme_mod( 'custom_logo' );
						$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );						

				    ?>
					<span class="login100-form-logo">
						<img src="<?php echo $image[0]; ?>" alt=""/>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						Reset Password?
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
				
						<input class="input100" type="text" name="user_pass" placeholder="New password" value="<?php if(isset($_GET['key']) && $_GET['action'] == "reset_pwd") { echo $new_password; } else { ""; } ?>">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>

					</div>


					<div class="container-login100-form-btn">
						<input type="hidden" name="reset-action" value="pwd_reset" />

						<button class="login100-form-btn" type="submit" name="reset_pass" value="Reset Password" id="submit">
							Reset Password
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>

<?php  get_footer(); ?>