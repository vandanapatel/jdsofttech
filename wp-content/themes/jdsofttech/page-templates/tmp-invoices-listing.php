<?php 
// Template Name:Invoices List


get_header();

global $post;

?>
<div class="row">
    <div class="col-md-12 single_delete_invoice text-right">
      <div class="add-new-invoice-btn"> <a href="<?php echo site_url('/add-new-invoice/'); ?>"> <i class="fas fa-plus"></i> Add New Invoice </a> </div>
  </div>
</div>

<div class="invoice-content">

  <?php
 
    $args = array(
      'post_type'   => 'invoice',
      'orderby'   => 'ID',
      'order'     => 'DESC',
      'posts_per_page' => -1
     );
     
    $wp_Query = new WP_Query( $args );
    if( $wp_Query->have_posts() ) :
    ?>
    <div class="table-invoicelist-wrap">
      <table id="table-invoicelist" class="display table table-striped table-invoicelist" style="width:100%">
          <thead>
            <tr class="row-name">
              <th width="10%"> Inv. No. </th>
              <th width="18%"> Cust. Name </th>
              <th width="13%"> Due Date </th>
              <th width="19%"> Payment Status </th>
			  <th width="15%"> Total Amount </th>
              <th width="25%" class="no-sort" > Actions </th>
            </tr>
          </thead>
        
          <tbody class="sticky_thead">
            <?php  
            $i = 1;
            while( $wp_Query->have_posts() ) :
              $wp_Query->the_post(); 
              global $status;
              ?>
			  
                <tr class="row-content">                
                    <td width="10%"><a href="<?php the_permalink(); ?>"><?php the_field('cust_invoice_number'); ?></a> </td>
                    <?php $customer = get_field('cust_invoice_customer'); ?> 
                    <td width="18%"><a href="<?php the_permalink( $customer->ID); ?>"> <?php echo $customer->post_title; ?></a> </td>
                    <td width="13%"><?php the_field('cust_invoice_duedate'); ?> </td>
                    <td width="19%" class="invoice_status">
                      <?php 
                        $inv_status = get_post_meta($post->ID, 'invoice_status', true);
                        if(!empty($inv_status)){                         
                          
                          if($inv_status == 'Sent'){ ?>
                            <span class="inv_mark_status"><?php echo $inv_status; ?></span>
                            <span class="markas_paid_check">                              
                              <input type="checkbox" name="inv_status" class="manual_autoinv_status" value="Paid" data-status="<?php echo $post->ID; ?>"> Mark as Paid
                            </span>
                          <?php 
                          }else{
                            echo $inv_status;
                          } 
                         
                        }else{
                          echo 'Created'; 
                        } 

                      ?>
                     
                    </td>
          					<td width="15%">$<?php echo the_field('cust_invoice_totalamount');?></td>
          					<td width="25%"> 

                      <!-- Edit Invocie -->

                      <a href="<?php echo site_url('/edit-invoice/').'?edit_Iid='. $post->ID; ?>" class="actions-btn btn-edit common-transition edit_invoice_cls" >
                        <i class="fas fa-edit"></i>        
                      </a> 

                      <!-- View Invoice -->
                     
                      <a href="javascript:void(0);" class="actions-btn btn-view common-transition view_invoice" title="Edit details" data-toggle="modal" data-viewid="<?php echo $post->ID; ?>" data-target="#viewModal">
                        <i class="fas fa-eye"></i>    
                      </a> 

                      <!-- Download Invoice PDF -->
                      
                      <a class="actions-btn btn-file-pdf common-transition pdf_invoice_<?php echo $post->ID; ?>" data-pdfid="<?php echo $post->ID; ?>" href="javascript:void(0);"> 
                        <i class="fa fa-file-pdf"></i>     
                      </a> 
                      
                      <!-- Email PDF -->

                      <a href="javascript:void(0);" class="actions-btn btn-envelope common-transition mail_invoice" title="Edit details" data-mailinvoiceid="<?php echo $post->ID; ?>" data-toggle="modal" data-target="#mailModal_<?php echo $i; ?>">
                        <i class="fas fa-envelope"></i>    
                      </a> 
                  
                      <script type="text/javascript">                        
                       
                        jQuery(document).ready(function(){
                          jQuery(".pdf_invoice_<?php echo $post->ID; ?>").click(function(e){
                            var pdfid = jQuery( this ).data("pdfid");
                            jQuery.ajax({
                                  url: ajaxurl,
                                  async: true,
                                  data: {action  : 'pdf_invoice',pdf_id: pdfid},
                                  success: function(data){                                        
                                  
                                    var newWin=window.open('','Print-Window');
                                    newWin.document.write(data);
                                    newWin.onload = function() {
                                        newWin.print();
                                    }

                                    newWin.document.close();
                                    setTimeout(function(){newWin.close();},500);
                                  
                                  }
                            }); 
                          });                           
                        });
                      </script>

                    </td>
                </tr>   
			
            <?php 

            // echo $post->ID;

            $i++;
            endwhile;
           wp_reset_postdata();
            ?>
                      <div id="modal" class="modal fade">
                        <div class="modal-dialog">
                          <div id="modal_target" class="modal-content">
                            
                          </div>
                        </div> 
                      </div>
          </tbody>            
          
      </table>
    </div>

  <?php
  else :
    esc_html_e( 'No Invoices!', 'jdsofttech' );
  endif;
  ?>
</div>

<?php get_footer(); ?>