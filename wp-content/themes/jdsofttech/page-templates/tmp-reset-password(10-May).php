<?php 
// Template Name:Reset Password

ob_start();
error_reporting(E_ALL);
ini_set("display_errors", 1);

get_header();




 
   if(isset($_GET['key']) && $_GET['action'] == "rp") {


		$reset_key = $_GET['key'];
		$user_login = $_GET['login'];
		$user_data = $wpdb->get_row($wpdb->prepare("SELECT ID, user_login, user_email FROM $wpdb->users WHERE user_activation_key = %s AND user_login = %s", $reset_key, $user_login));
		$user_login = $user_data->user_login;
		$user_email = $user_data->user_email;
		if(!empty($reset_key) && !empty($user_data)) {
			$new_password = wp_generate_password(7, false); //you can change the number 7 to whatever length needed for the new password
			wp_set_password( $new_password, $user_data->ID );
			//mailing the reset details to the user
			$message = __('Your new password for the account at:') . "\r\n\r\n";
			$message .= get_bloginfo('name') . "\r\n\r\n";
			$message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
			$message .= sprintf(__('Password: %s'), $new_password) . "\r\n\r\n";
			$message .= __('You can now login with your new password at: ') . get_option('siteurl')."/log-in" . "\r\n\r\n";
		 
			if ( $message && !wp_mail($user_email, 'Password Reset Request', $message) ) {
				echo "<div class='error'>Email failed to sent for some unknown reason</div>";
				exit();
			}
			else {
				$redirect_to = get_bloginfo('url')."/log-in?action=reset_success";
				wp_safe_redirect($redirect_to);
				exit();
			}
		}
		else exit('Not a Valid Key.');
	}


?>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/bg-01.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form resetpassform" id="resetpassform"  method="post" action="<?php echo site_url('log-in/'); ?>?action=reset_success">
					<input type="hidden" id="user_login" value="<?php echo $user_login; ?>" autocomplete="off">
					<?php 

						$custom_logo_id = get_theme_mod( 'custom_logo' );
						$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
						

				    ?>
					<span class="login100-form-logo">
						<img src="<?php echo $image[0]; ?>" alt=""/>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						Reset Password?
					</span>
					<span>
						
					</span>
					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						 
						<input class="input100" type="text" name="user_password" placeholder="New Password" value="<?php if(isset($_GET['key']) && $_GET['action'] == "rp") { echo $new_password; } ?>">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>

					</div>


					<div class="container-login100-form-btn">
						<input type="hidden" name="reset_key" value="" value="<?php echo $reset_key; ?>" />
						<button class="login100-form-btn" name="reset_pass" type="submit" value="Reset Password" id="submit">
							Reset Password
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>

<?php  get_footer(); ?>