<?php 
get_header();
	
global $post;

?>


	<div class="row button_bottom_space">
		<div class="col-md-12 back_invoice text-left">
			<a href="<?php echo site_url('/customers/'); ?>" class="btn a-btn-slide-text">	
				<span> <i class="fas fa-long-arrow-alt-left"></i><strong>Back</strong></span>            
			</a>
		</div>
	</div>


  	<div class="supplier-customer-box-row  customer-details">
	    <div class="table-responsive-md">
	      	<table width="0" border="0" cellpadding="0" cellspacing="0">	      		
		        <tbody>
		          <tr>
		          	<?php 
		          	if( have_rows('customer_address') ): 
  						while ( have_rows('customer_address') ) : the_row();
  						?>
			            <td class="left-colum">
			            	<div class="supplier"> <span> Company Name: <?php the_field('customer_company_name'); ?> </span> <a href="javacript:void(0);"> 
							<?php the_title(); ?>. </a>
			                	<p> Web & Software Developments Company </p>
			              	</div>
			              	<?php 

			              		$street = get_sub_field('customer_street'); 
			                	$zipcode = get_sub_field('customer_zipcode');
			                	$country = get_sub_field('customer_country');
			                	$state = get_sub_field('customer_state');
			                	$city = get_sub_field('customer_city');			               			
			               		
								?>

				              	<div class="address">

		              	            <div class="row">
                                		<div class="col-md-6">  

	                                        <span> Address </span>
						               		<p> <?php echo $street .', '. $city .',</br>'. $zipcode .', ' .$state. ',</br> '. $country; ?></p>

                                    	</div>
	                                    <div class="col-md-6">  
							                <ul>
							                  <li> <span> Phone: </span> <a href="#"><a href="tel:<?php the_field('customer_phone_no'); ?>"><?php the_field('customer_phone_no'); ?></a>
							                    </a> </li>
							                  <li> <span> Website: </span> <a href="#"> www.jdsofttech.com </a> </li>
							                  <li> <span> E-mail: </span> <a href="mailto:<?php the_field('customer_email'); ?>"> <?php the_field('customer_email'); ?> </a> </li>
							                </ul>
							            </div>
							        </div>

					            </div>
			          	</td>
			          	<?php 
			          	endwhile;
			      	endif;
			          	?>
		         
		          </tr>
		        </tbody>
	      	</table>
	    </div>
 	</div>
 </div>
<?php get_footer(); ?>