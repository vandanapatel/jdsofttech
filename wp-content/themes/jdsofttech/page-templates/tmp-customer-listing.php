<?php 
// Template Name:Customers List


get_header();

global $post;

?>
<div class="row">
  <div class="col-md-12 single_delete_invoice text-right">
    <div class="add-new-invoice-btn"> <a href="<?php echo site_url('/add-new-customer/'); ?>"> <i class="fas fa-plus"></i> Add New Customer </a> </div>
  </div>
</div>

<div class="invoice-content">
  <?php

  $args = array(
    'post_type'   => 'customer',
    'orderby'   => 'ID',
    'order'     => 'DESC',
    'posts_per_page' => -1
   );
   
  $wp_Query = new WP_Query( $args );
  if( $wp_Query->have_posts() ) :
  ?>
    <div class="table-invoicelist-wrap">
 
      <table id="table-invoicelist" class="display table table-striped table-invoicelist" style="width:100%">
        <thead>
          <tr>
            <th width="10%"> No. </th>
            <th width="15%"> Customer Name </th>
            <th width="30%"> Email Id </th>
            <th width="15%"> Country </th>
            <th width="15%"> City </th>
            <th  width="15%" class="no-sort" > Actions </th>
          </tr>
        </thead>
        <tbody>

        <?php  $i = 1;
          while( $wp_Query->have_posts() ) :
            $wp_Query->the_post(); ?>

              <tr>                
                  <td width="10%"><?php echo $i; ?></a> </td>
                  <td width="15%"> <a href="<?php the_permalink(); ?>"> 
				  <?php the_field('customer_firstname');echo ' ';the_field('customer_lastname'); ?>  
				  <?php //the_field('customer_name'); ?></a> </td>
                  <td width="30%"> <?php the_field('customer_email'); ?> </td>
                  <?php if( have_rows('customer_address') ): 
                    while ( have_rows('customer_address') ) : the_row(); ?>
                     
                      <td width="15%"> <?php the_sub_field('customer_country'); ?> </td>
                      <td width="15%"> <?php the_sub_field('customer_city'); ?> </td>

                    <?php 
                    endwhile;
                  endif;
                  ?>
                  <td width="15%"> 
                      <!-- Edit Invocie -->

                      <a href="<?php echo site_url('/edit-customer/').'?edit_Cid='. $post->ID; ?>" class="actions-btn btn-edit common-transition edit_customer_cls" >
                        <i class="fas fa-edit"></i>        
                      </a> 

                      <!-- View Invoice -->
                     
                      <a href="javascript:void(0);" class="actions-btn btn-view common-transition view_invoice" title="Edit details" data-toggle="modal" data-target="#viewModal_<?php echo $i; ?>">
                        <i class="fas fa-eye"></i>    
                      </a> 

                     

                      <!-- View Model Start-->  
                      <div class="modal fade" id="viewModal_<?php echo $i; ?>" role="dialog">
                        <div class="modal-dialog" role="document">
                        
                          <!-- Modal content -->
                          <div class="modal-content">

                            <div class="modal-header">
                              <h3 class="modal-title" style="">Customer Detail</h3>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              
                            </div>

                            <div class="modal-body modal-tab-border">

                              <table class="table dtr-details" width="100%">
                                <tbody>
                                  <?php 
                                  $email = get_field('customer_email');
                                  $phone = get_field('customer_phone_no');
                                  $invdate = get_field('cust_invoice_invdate');
                                  $duedate = get_field('cust_invoice_duedate'); ?>
                                  
                                  <tr data-dt-row="4" data-dt-column="0"><td> Customer Name :</td><td> <?php echo $post->post_title; ?></td></tr>
                                  <tr data-dt-row="4" data-dt-column="1"><td> Customer Email :</td><td> <?php echo $email; ?> </td></tr>
                                  <?php 
                                  if( have_rows('customer_address')): 
                                    while ( have_rows('customer_address') ) : the_row(); 
                        
                                    $country = get_sub_field('customer_country');
                                    $state = get_sub_field('customer_state');
                                    $city = get_sub_field('customer_city'); ?>

                                    <tr data-dt-row="4" data-dt-column="2"><td> Country :</td><td> <?php echo $country; ?> </td></tr>
                                    <tr data-dt-row="4" data-dt-column="3"><td> State  :</td><td> <?php echo $state; ?> </td></tr>
                                    <tr data-dt-row="4" data-dt-column="4"><td> City  :</td><td> <?php echo $city; ?> </td></tr>
                                    <?php 
                                    endwhile;
                                    //wp_reset_postdata();
                                  endif; ?>

                                  <tr data-dt-row="4" data-dt-column="5"><td> Phone No. :</td><td> <?php echo $phone; ?> </td></tr>   
                                    
                                </tbody>
                              </table>

                            </div>

                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                      <!-- View Model End -->
                    </td>
              </tr>
          
          <?php 
          $i++;
          endwhile;
          wp_reset_postdata();
          ?>

        </tbody>            
        
      </table>
      
    </div>

  <?php
  else :
    esc_html_e( 'No Customers!', 'jdsofttech' );
  endif;
  ?>
</div>


<?php get_footer(); ?>