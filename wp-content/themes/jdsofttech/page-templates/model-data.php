<?php 
 require_once("wp-load.php");

	// echo $_GET['pdf_Iid'];
 	//if(isset($_POST['pdfme'])){ ?>


<?php echo "Hello"; echo $_REQUEST['viewinvoiceid']; die(); ?>
 	 
    <!-- View Model Start-->  

    <div class="modal fade" id="viewModal_<?php echo $i; ?>" role="dialog">
      <div class="modal-dialog" role="document">
      
        <!-- Modal content -->
        <div class="modal-content">

          <div class="modal-header">
            <h3 class="modal-title" style="">Invoice : #<?php the_field('cust_invoice_number'); ?></h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            
          </div>

          <div class="modal-body modal-tab-border">

            <table class="table dtr-details" width="100%">
              <tbody>
                <?php 
                $email = get_field('customer_email', $customer->ID);
                $phone = get_field('customer_phone_no', $customer->ID);
                $invdate = get_field('cust_invoice_invdate');
                $duedate = get_field('cust_invoice_duedate'); ?>
                
                <tr data-dt-row="4" data-dt-column="0"><td> Customer Name :</td><td> <?php echo $customer->post_title; ?></td></tr>
                <tr data-dt-row="4" data-dt-column="1"><td> Customer Email :</td><td> <?php echo $email; ?> </td></tr>
                <?php 
                if( have_rows('customer_address', $customer->ID) ): 
                  while ( have_rows('customer_address', $customer->ID) ) : the_row(); 
      
                  $country = get_sub_field('customer_country', $customer->ID);
                  $state = get_sub_field('customer_state', $customer->ID);
                  $city = get_sub_field('customer_city', $customer->ID); ?>

                  <tr data-dt-row="4" data-dt-column="2"><td> Country :</td><td> <?php echo $country; ?> </td></tr>
                  <tr data-dt-row="4" data-dt-column="3"><td> State  :</td><td> <?php echo $state; ?> </td></tr>
                  <tr data-dt-row="4" data-dt-column="4"><td> City  :</td><td> <?php echo $city; ?> </td></tr>
                  <?php 
                  endwhile;
                  //wp_reset_postdata();
                endif; ?>

                <tr data-dt-row="4" data-dt-column="5"><td> Phone No. :</td><td> <?php echo $phone; ?> </td></tr>   
                <tr data-dt-row="4" data-dt-column="6"><td> Invocie Date  :</td><td> <?php echo $invdate; ?> </td></tr>
                <tr data-dt-row="4" data-dt-column="7"><td> Due Date  :</td><td> <?php echo $duedate; ?> </td></tr>   
                <tr data-dt-row="4" data-dt-column="8"><td> Websites  :</td>
                  <td> 
                   
                    <table width="100%">
                      <thead>
                        <th>Desc.</th>
                        <th>Quantity</th>
                        <th>Rate</th>
                        <th>Amount</th>
                      </thead>
                      <tbody>
                        <?php 
                       
                        if( have_rows('cust_invoice_website') ): 
                          while ( have_rows('cust_invoice_website') ) : the_row(); ?>
                            <tr>
                              <td><?php the_sub_field('cust_invoice_desc', $post->ID); ?></td>
                              <td><?php the_sub_field('cust_invoice_quantity'); ?></td>
                              <td><?php the_sub_field('cust_invoice'); ?></td>
                              <td>$<?php the_sub_field('cust_invoice_amount'); ?></td>
                            </tr>
                          <?php 
                          endwhile;
                          // /  wp_reset_postdata();
                        endif; ?>
                      </tbody>
                    </table>
                  </td>
                </tr>   
              </tbody>
            </table>

          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        
      </div>
    </div>
     
    <!-- View Model End -->

    <!-- Mail Model Start--> 
    <div class="modal fade" id="mailModal_<?php echo $i; ?>" role="dialog">
      <div class="modal-dialog" role="document">
      
        <!-- Modal content -->
        <div class="modal-content">

          <div class="modal-header">
            <h3 class="modal-title" style="">Mail To Customer or Email</h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            
          </div>

          <div class="modal-body modal-tab-border">

            <form action="" method="POST">
              
              <table class="table invoice_email_table" width="100%">
                <tbody>
                  <?php 

                  $email = get_field('customer_email', $customer->ID); 
                  $cust_name = get_field('customer_name', $customer->ID); 
                  $cust_slug = $customer->post_name;

                  ?>
                  <tr>
                    <td width="30%"> Mail to :</td>
                    <td width="70%">  
                      <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default active"><input type="radio" name="radio_invoice_email" value="radio_customer_email" class="radio_email_<?php echo $post->ID; ?>" id="radio_customer" checked=""> Customer Email </label>
                        <label class="btn btn-default"><input type="radio" name="radio_invoice_email" value="radio_custom_email" class="radio_email_<?php echo $post->ID; ?>" id="radio_custom"> Custom Email</label>
                      </div>
                    </td>
                  </tr>
                  <tr class="customer-email">
                    <td width="30%"> Customer Email :</td>
                    <td width="70%"> <?php echo $email; ?> </td>
                  </tr>
                  <tr class="custom-email">
                    <td width="30%"> Custom Email : </td>
                    <td width="70%"><input type="text" name="custom_emailid" class="custom_emailid"></td></tr>
                  <tr>
                    <td width="100%" colspan="2">
                      <input type="submit" value="SEND" class="submit_mail btn btn-default" data-mail-invoiceid="<?php echo $post->ID; ?>" name="submit_email_<?php echo $post->ID; ?>">
                    </td>
                  </tr>   

                </tbody>
              </table>
                <?php 
                  $invoice_url = get_permalink($post->ID);        
                  $encode_string = $post->ID . $cust_slug;
                  $activate_invoice = base64_encode($encode_string); 
                ?>
              
              <input type="hidden" name="mail_invoice_url" value="<?php echo get_tiny_url($invoice_url) .'?key='. $activate_invoice; ?>">
               <input type="hidden" name="mail_invoice_activatekey" value="<?php echo $activate_invoice; ?>">
              
              <?php  get_template_part('page-templates/mail-invocie'); ?>
            </form>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        
      </div>
    </div>      

    <!-- Mail Model End--> 
<?php // } 

function view_invoice_ajaxdata1(){
  
  $invoiceid = $_REQUEST['viewinvoiceid'];  
  $inoivce_no = get_field('cust_invoice_number',$invoiceid);
  $sel_customer = get_field('cust_invoice_customer', $invoiceid);
  
  
  $email = get_field('customer_email', $sel_customer->ID);
  $phone_no = get_field('customer_phone_no', $sel_customer->ID);  
  $invoice_date = get_field('cust_invoice_invdate', $invoiceid);  
  $due_date = get_field('cust_invoice_duedate', $invoiceid);

// echo wp_send_json($phone_no);

  global $websites, $total, $tbodyend, $detailtable, $address;
    if( have_rows('customer_address', $sel_customer->ID) ): 
      while ( have_rows('customer_address', $sel_customer->ID) ) : the_row();
          $street = get_sub_field('customer_street', $sel_customer->ID);
          $zipcode = get_sub_field('customer_zipcode', $sel_customer->ID);
          $state = get_sub_field('customer_state', $sel_customer->ID);
          $city = get_sub_field('customer_city', $sel_customer->ID);
          $country = get_sub_field('customer_country', $sel_customer->ID);
          // $country = $countryarr['label'];
     
          $address = '<tr data-dt-row="4" data-dt-column="2"><td> Country :</td><td>'. $country .'</td></tr>
            <tr data-dt-row="4" data-dt-column="3"><td> State  :</td><td>'. $state .'</td></tr>
            <tr data-dt-row="4" data-dt-column="4"><td> City  :</td><td>'. $city .'</td></tr>';                 
      endwhile;
    endif;

    if( have_rows('cust_invoice_website',$invoiceid) ):    
      while ( have_rows('cust_invoice_website',$invoiceid) ) : the_row();    
          $websites = '<tr>
                <td>'. get_sub_field('cust_invoice_desc', $invoiceid) .'</td>
                <td>'. get_sub_field('cust_invoice_quantity', $invoiceid) .'</td>
                <td>'. get_sub_field('cust_invoice', $invoiceid) .'</td>
                <td>$'. get_sub_field('cust_invoice_amount', $invoiceid) .'</td>
            </tr>';
    $total += intval( get_sub_field('cust_invoice_amount',$postid));
    endwhile;

    $tbodyend = '<tfoot>
          <tr>
            <td  colspan="3"><div class="total">Total</div></td>
            <td><div class="total amount"> $'. $total .'</div></td>
          </tr>
        </tfoot>';                  
    endif;
    
    $detailtable ='<table class="table dtr-details" width="100%">
        <tbody>
       
          <tr data-dt-row="4" data-dt-column="0"><td> Customer Name :</td><td>'. $sel_customer->post_title .'</td></tr>
          <tr data-dt-row="4" data-dt-column="1"><td> Customer Email :</td><td>'. $email .'</td></tr>
           '. $address .'
          <tr data-dt-row="4" data-dt-column="5"><td> Phone No. :</td><td>'. $phone_no .'</td></tr>
          <tr data-dt-row="4" data-dt-column="6"><td> Invocie Date  :</td><td>'. $invoice_date .'</td></tr>
          <tr data-dt-row="4" data-dt-column="7"><td> Due Date  :</td><td>'. $due_date .'</td></tr>   
          <tr data-dt-row="4" data-dt-column="8"><td> Websites  :</td>
            <td>              
              <table width="100%">
                <thead>
                  <th>Desc.</th>
                  <th>Quantity</th>
                  <th>Rate</th>
                  <th>Amount</th>
                </thead>
                <tbody>'. $websites . '</tbody>'. $tbodyend .'</table>
            </td>
          </tr>   
        </tbody>
    </table>';
  $html = '<div class="modal fade" id="viewModal" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                <div class="modal-header">
                            <h3 class="modal-title" style="">Invoice : #'.$inoivce_no.'</h3>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body modal-tab-border">
              '. $detailtable .'
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>';
  echo wp_send_json($html);
}



$emailid = 'submit_email_'.$post->ID;
if(isset($_POST[$emailid])){
   
    $invoice_id = $post->ID;

    $invoice_url = $_POST['mail_invoice_url']; 

    $customer = get_field('cust_invoice_customer', $invoice_id);

    $custom_email = $_POST['custom_emailid'];

    if( $_POST['radio_invoice_email'] == 'radio_custom_email' ){

       $to = $_POST['custom_emailid'];

    }elseif( $_POST['radio_invoice_email'] == 'radio_customer_email'){

       $to = get_field('customer_email', $customer->ID);

    }
    
    $cust_slug = $customer->post_name;
    $encode_string = $invoice_id . $cust_slug;
    $activate_invoice = base64_encode($encode_string); 

    update_post_meta( $invoice_id, "single_invoice_activate_key", $activate_invoice  ); 
    // $to = "nbhalala.jd@gmail.com";
    $subject = "Invoice Mail";

    $txt = '<!DOCTYPE html>
          <html lang="en">
            <head>
              <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <style>
               h2{
                  font-size: 20px;
                  font-weight:300px;
               }
               p{
                  font-size: 17px;
                  font-weight:300px;
               }
               a{
                  font-size: 17px;
                  font-weight:300px;
                  text-decoration:none;
               }
               
              </style>          
            </head>
            <body>
            <h2 style="font-size: 20px;font-weight:300px">Hello ' .get_field('customer_name', $customer->ID).',<h2>
            <p style="font-size:17px;font-weight:300px;">Your Invoice URL is Generated, please follow bellow link.</p>
            <a href="'.$invoice_url.'" target="_blank" style="font-size:17px;font-weight:300px;">' .$invoice_url.'</a>
          </body></html>';

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html; charset=iso-8859-1" . "\r\n";

    $headers .= "From: vpatel@taskme.biz";

    wp_mail($to,$subject,$txt,$headers);

}
?>



