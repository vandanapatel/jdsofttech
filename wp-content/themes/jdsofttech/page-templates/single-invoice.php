<?php 
get_header();

global $post;

?>
<section class="content-section single_invoice_div" style="margin: 70px 30px 50px;">
  	<div class="top-heading">
    	<h1> Invoice number: <?php the_field('cust_invoice_number'); ?> </h1>
  	</div>

  	<div class="logo-block-row"> 
  		<a class="logo" href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="JDsofttech"/> </a> 
 	</div>

	<?php 

	$activation_key = get_post_meta(get_the_ID(), 'single_invoice_activate_key', true );
	
	if(!isset($_GET['key'])) { ?>
 	<div class="row button_bottom_space">
		<div class="col-md-6 text-left back_invoice">		   
			<a href="<?php echo site_url('/invoices/'); ?>" class="btn a-btn-slide-text">
            	<span> <i class="fas fa-long-arrow-alt-left"></i><strong>Back</strong></span>            
			</a>			
	 	</div>
	 	<div class="col-md-6 text-right delete_invoice">		   
	 		<?php $link =  wp_nonce_url( get_bloginfo('url') . "/wp-admin/post.php?action=delete&amp;post=" . $post->ID, 'delete-post_' . $post->ID); ?>
	 		<a href="<?php echo $link; ?>" class="btn a-btn-slide-text">
		       <span> <i class="fas fa-times"></i><strong>Delete</strong></span>            
		    </a>
	 	</div>
		 
 	</div>

 <?php } ?>

  	<div class="supplier-customer-box-row">
	    <div class="table-responsive-md">
	      	<table width="0" border="0" cellpadding="0" cellspacing="0">
		        <tbody>
		          <tr>
		            <td class="left-colum">
		            	<div class="supplier"> 
		                	<p> Web & Software Developments Company </p>
		              	</div>
		              	<div class="address"> <span> Address </span>
			                <p> 903 Main Street, Hanson,</br>
			                  02341,MA,</br>
			                  USA </p>
			                <ul>
			                  <li> <span> Phone: </span> <a href="#">
			                    <p>+1 (781) 9744568 </p>
			                    </a> </li>
			                  <li> <span> Website: </span> <a href="https://www.jdsofttech.com/"> www.jdsofttech.com </a> </li>
			                  <li> <span> E-mail: </span> <a href="mailto:info@jdsofttech.com"> info@jdsofttech.com </a> </li>
			                </ul>
			             </div>
		          	</td>
		            <td class="right-culum">
		            	<?php 
		                $customer = get_field('cust_invoice_customer');
		                if( have_rows('customer_address', $customer->ID) ): 
	      					while ( have_rows('customer_address', $customer->ID) ) : the_row(); 
	      						$customer_firstname = get_field('customer_firstname', $customer->ID);
	      						$customer_lastname = get_field('customer_lastname', $customer->ID);
	      						$cust_name = $customer_firstname . ' '. $customer_lastname;
	      						?>
				            	<div class="customer"> 
									<span> Company Name:  <?php the_field('customer_company_name', $customer->ID); ?></span>
				            		<span> Customer: </span> <a href="<?php echo get_permalink($customer->ID); ?>"> <?php echo $cust_name; ?> </a>
					                
					                <p>
						               	<?php 

					                	$street = get_sub_field('customer_street', $customer->ID); 
					                	$zipcode = get_sub_field('customer_zipcode', $customer->ID);
					                	$state = get_sub_field('customer_state', $customer->ID);
					                	$city = get_sub_field('customer_city', $customer->ID);
					                	$country = get_sub_field('customer_country', $customer->ID);
														
					                	echo $street. ', ' .$city . ' </br>';
					                	echo $zipcode. ', ' . $state . ', '.$country.'</br>';
				                 		
				                 		?>	
					               		Ph : <?php the_field('customer_phone_no', $customer->ID); ?>
					               	</p>				               	
					              
				              	</div>
		              	 	<?php 
		              	 	endwhile;
		                endif;
			            ?>
		          	</td>
		          </tr>
		        </tbody>
		        <tfoot>
		          <tr>
		          	<?php 
		          		$invoice_date = get_field('cust_invoice_invdate'); 
		          		$due_date = get_field('cust_invoice_duedate'); 

		          	?>
		            <td class="left-colum foot-left-col"> Date : <?php echo $invoice_date; ?> </td>
		            <td class="right-colum foot-right-col"> Due Date : <?php echo $due_date; ?> </td>
		          </tr>
		        </tfoot>
	      	</table>
	    </div>
 	</div>
  	<div class="description-amount-table-row">
	    <div class="table-responsive-sm">
	      <table class="table-striped">
	      	<thead>
	          	<tr>
		            <th class="col-description"> Description </th>
		            <th class="col-description"> Quantity </th>
		            <th class="col-description"> Rate </th>
		            <th class="col-amount"> Amount </th>
	          	</tr>
	        </thead>
				<?php 		       
       		if( have_rows('cust_invoice_website') ):  ?>

		        <tbody>
					<?php global $total,$desc,$description,$qua; 
						while ( have_rows('cust_invoice_website') ) : the_row(); 
						$cust_invoice_desc =  get_sub_field('cust_invoice_desc');
						$cust_invoice_quantity =  get_sub_field('cust_invoice_quantity');
						$cust_invoice =  get_sub_field('cust_invoice');
						$cust_invoice_amount =  get_sub_field('cust_invoice_amount');?>
				        <tr>	
				        <?php if(!empty($cust_invoice_desc)):?> <td class="col-description"><?php echo $cust_invoice_desc; ?> </td> <?php endif;?>
						<?php if(!empty($cust_invoice_quantity)):?>	<td class="col-description"><?php echo $cust_invoice_quantity;?> </td> <?php endif;?>
				        <?php if(!empty($cust_invoice)):?> <td class="col-description"><?php  echo '$'.$cust_invoice;?> </td> <?php endif;?>
				        <?php if(!empty($cust_invoice_amount)):?> <td class="col-amount"><?php echo '$'.$cust_invoice_amount; ?> </td> <?php endif;?>
				        </tr>

				        <tr>
				            <td class="col-description"></td>						            
				            <td class="col-description"></td>
				            <td class="col-description"> </td>
				            <td class="col-amount"></td>
				        </tr>
							 
					<?php 
					
					$total += intval( get_sub_field('cust_invoice_amount'));
					$desc .=  get_sub_field('cust_invoice_desc')."," ;
					
					$qua += intval( get_sub_field('cust_invoice_quantity'));
	  			   	endwhile; ?>
					
	  			</tbody>  
          	<?php  
          	endif; ?>
			 
	        <tfoot>
	          <tr>
	            <td  colspan="3"><div class="total">Total</div></td>
	            <td><div class="total amount"> $<?php echo $total; ?></div></td>
	          </tr>	  
	        </tfoot> 
			
		</table>
		<div class="text-right">
				  <input type="radio" name="send_check" value="Send Check">Send Check	
				  <div class="pay-address">
					<p>JD Softtech Inc
					903 Main Street, 
					Hanson, MA, 02341
					USA</p>
				  </div>
		</div>		

		<?php 
		global $description;
		$title = get_field('cust_invoice_number'); 
	
		?>
			
		<form method="post" action="https://www.payjunctionlabs.com/trinity/quickshop/add_to_cart_snap.action">
			<input type="hidden" name="description" value="<?php echo (!empty($title)) ?  'Invoice: #'.$title : 'Product Invoice Detail';?>">
			<input type="hidden" name="need_to_ship" value="no">
			<input type="hidden" name="need_to_tax" value="no">
			<input type="hidden" name="identifier" value="Invoice Detail">
			<input type="hidden" name="price" value="<?php echo $total; ?>">
			<input type="hidden" name="store" value="jdsofttech"> 
			<input type="hidden" name="invoice_id" value="<?php echo $post->ID; ?>">
			<table>
				<tbody> 
					<tr>
						<td align="left">
							<input type="submit" name="submit" value="Pay Now" class="checkout-btn">
						</td>
					</tr>
				</tbody> 
			</table>
		</form>
	    </div>
  	</div>
  	<div class="contenteditable">
	    <p> Amount in words: <?php echo convertNum(  $total ); ?> Dolloars Only. </p>
	    <p> Declaration: All amounts shown are in USD.</p>
  	</div>
  	<div class="thank-you-text"> Thank you for your business! </div>
</section>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script type="text/javascript">
$(document).ready(function(){
	 $('.pay-address').hide();
    $('input[type="radio"]').click(function(){
        $('.pay-address').show();
    });
});
</script>
<?php get_footer(); ?>