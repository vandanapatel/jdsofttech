<?php 
// Template Name:Edit Customer


acf_form_head();  
get_header();

?>
	<div class="row button_bottom_space">
		<div class="col-md-12 back_invoice text-left">
			<a href="<?php echo site_url('/customers/'); ?>" class="btn a-btn-slide-text">	
				<span> <i class="fas fa-long-arrow-alt-left"></i><strong>Back</strong></span>            
			</a>
		</div>
	</div>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header cusotm_form_header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->

		<?php jdsofttech_post_thumbnail(); ?>

		<div class="entry-content">


			<?php
			# Get the the post_id from the url
			if(isset($_GET['edit_Cid'])){

				$post_id = $_GET['edit_Cid'];
			
			}

			acf_form(array(
				'post_id'		=> $post_id,
				'form'               => true,
				'field_groups' => array('198'),
				//'fields' => array('field_55fbd2ec6359f', 'field_560e37f2d8763'),
				'new_post'		=> array(
					'post_type'		=> 'customer',
					'post_status'	=> 'publish'
				),
				//'return'		=> site_url('/invoices/'),
				'return'             => '%post_url%', // Redirect to new post url
				'submit_value'	=> 'Submit',
				'updated_message'    => 'Saved!',
				'form'               => true,
			));
			
			?>
		</div><!-- .entry-content -->

	</article><!-- #post-<?php the_ID(); ?> -->


<?php get_footer(); ?>