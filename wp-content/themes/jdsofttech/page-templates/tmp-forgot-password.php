<?php 
// Template Name:Forgot Password

if( is_user_logged_in() ) {
	wp_redirect(home_url());
 	exit();
}	

ob_start();
error_reporting(E_ALL);
ini_set("display_errors", 0);

get_header();

global $wpdb, $err; 

if($_POST){
 
    if($_POST['action_forgot'] == "pwd_reset" && isset($_POST['forgot_pass']) ){
		

		$user_login = $wpdb->escape(trim($_POST['user_login']));

		if ( !wp_verify_nonce( $_POST['pwd_nonce'], "pwd_nonce")) {
		  exit("No trick please");
		}

		if(empty($_POST['user_login'])) {

			$err = '<span class="login-error-cls">Please enter your Username or E-mail address</span>';
		}
		
		 
		else if ( strpos($user_login, '@') ) {

			$user_data = get_user_by_email($user_login);

			if(empty($user_data)) {

				$err = '<span class="login-error-cls">Invalid E-mail address!</span>';
		
			}
			
		}
		else {
			$user_data = get_userdatabylogin($user_login);

			if(empty($user_data) || $user_data->caps['administrator'] == 1) {
			
			$err = '<span class="login-error-cls">Invalid Username!</span>';
			
			}
		}
		 

		$user_login = $user_data->user_login;
		$user_email = $user_data->user_email;

		$key = $wpdb->get_var($wpdb->prepare("SELECT user_activation_key FROM $wpdb->users WHERE user_login = %s", $user_login));

		if(empty($key)) {
		
			$key = wp_generate_password(20, false);
			$wpdb->update($wpdb->users, array('user_activation_key' => $key), array('user_login' => $user_login));
		}
		 
	
		$message = __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";
		$message .= get_option('siteurl') . "\r\n\r\n";
		$message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
		$message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
		$message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
		$message .= site_url() . "/reset-password/?action=reset_pwd&key=$key&login=" . rawurlencode($user_login) . "\r\n";

		if ( wp_mail($user_email, 'Password Reset Request', $message) ) {

			$err = '<div class="success login-error-cls">We have just sent you an email with Password reset instructions.</div>';
		}
		
	}   
} 

?>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/bg-01.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form" id="lostpasswordform" action="" method="post">
					<?php 

						$custom_logo_id = get_theme_mod( 'custom_logo' );
						$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
						

				    ?>
					<span class="login100-form-logo">
						<img src="<?php echo $image[0]; ?>" alt=""/>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						Lost Your Password?
					</span>
					<span>

						<?php 
						if($_POST){
							
							echo $err;
						}
		?>
						
					</span>
					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						 <?php $user_login = isset( $_POST['user_login'] ) ? $_POST['user_login'] : ''; ?>

						<input class="input100" type="text" name="user_login" placeholder="Username or Email Address" value="<?php echo $user_login; ?>">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>

					</div>

					 

					<div class="container-login100-form-btn">
						<input type="hidden" name="action_forgot" value="pwd_reset" />
						<input type="hidden" name="pwd_nonce" value="<?php echo wp_create_nonce("pwd_nonce"); ?>" />
						<button class="login100-form-btn" type="submit" name="forgot_pass" value="Get New Password" id="submit">
							Get New Password
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>

<?php  get_footer(); ?>