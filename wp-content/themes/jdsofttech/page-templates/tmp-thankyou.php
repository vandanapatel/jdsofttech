<?php 
// Template Name:Thank you 


get_header();

// echo '<pre>';
// print_r($_POST);
// echo '</pre>';

if( isset($_GET['status'])){

	$invoice_id= $_POST['invoice_id'];
	$status = 'Paid';
	
	if ( $_POST['response_code'] == 00 ) {
		update_post_meta($invoice_id, 'invoice_status', $status);
	}else{
		add_post_meta($invoice_id, 'invoice_status', $status, true);
	}
}

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>