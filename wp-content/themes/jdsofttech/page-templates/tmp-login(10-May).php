<?php 
// Template Name:Login Page

// if( is_user_logged_in() ) {
// 	wp_redirect(home_url());
//  	exit();
// }

ob_start();
error_reporting(E_ALL);
ini_set("display_errors", 1);

get_header();

if($_POST) {
 
	global $wpdb, $remember;
	$err = '';
	$success = '';
 
	//We shall SQL escape all inputs
	$username = $wpdb->escape($_REQUEST['username']);
	$password = $wpdb->escape($_REQUEST['pass']);

	if(isset($_POST['rememberme'] )){
		$remember = $wpdb->escape($_POST['rememberme']);
		if($remember){ 
	
			$remember = "true";
		
		}else{ 
		
			$remember = "false"; 
		
		}
 	}	
 
	$login_data = array();
	$login_data['user_login'] = $username;
	$login_data['user_password'] = $password;
	$login_data['remember'] = $remember;
 
	$user_verify = wp_signon( $login_data, false ); 
 
 

}
?>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/bg-01.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="post">
					<?php 

						$custom_logo_id = get_theme_mod( 'custom_logo' );
						$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
						

				    ?>
					<span class="login100-form-logo">
						<img src="<?php echo $image[0]; ?>" alt=""/>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						Log in
					</span>

					<span class="login-error-cls">
						<?php 
						if($_POST) {
	 
							if ( is_wp_error($user_verify) ) 
							{
							  	echo  $err = $user_verify->get_error_message();
							} else {	

								wp_set_current_user( $user_verify->ID, $username );
								do_action('set_current_user');
							//	wp_redirect(home_url());
							} 
						}

						?>
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<div class="contact100-form-checkbox">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="rememberme">
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit" value="Login" >
							Login
						</button>
					</div>

					<input type="hidden" name="task" value="login" />

					<div class="text-center p-t-90">
						<a class="txt1" href="<?php echo site_url('/forgot-password/'); ?>">
							Forgot Password?
						</a>
					</div>

				</form>
			</div>
		</div>
	</div>

<?php  get_footer(); ?>