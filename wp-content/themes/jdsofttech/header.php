<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Jdsofttech
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">


	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php if(get_post_type( get_the_ID() ) != 'invoice' && !is_page_template( 'page-templates/tmp-login.php' ) && !is_page_template( 'page-templates/tmp-forgot-password.php' ) &&  !is_page_template( 'page-templates/tmp-reset-password.php' ) ){ ?>

 	<header class="header">
	    <div class="container"> 
	    <?php 

			$custom_logo_id = get_theme_mod( 'custom_logo' );
			$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
			

	    ?>
	    <a class="logo" href="<?php echo site_url(); ?>"> <img src="<?php echo $image[0]; ?>" alt=""/> </a>
	    
	    <div class="menu">
	    <?php

	    	if(!is_user_logged_in()){

		    	$login_logout = '<a href="'.site_url('log-in').'"> Login <img src="'. get_template_directory_uri() .'/assets/img/login.png" alt=""/></a>';

	    	} else {

	    		$login_logout = '<a href="' .wp_logout_url(). '"> Log Out<img src="'. get_template_directory_uri() .'/assets/img/logout.png" alt=""/></a>';

	    	}
		    wp_nav_menu(
		      array (
		        'theme_location'  => 'header-menu',
		        'container'       => '',
		        'container_class' => '',
		        'container_id'    => '',
		        'menu_class'      => 'menu',
		        'echo'            => true,
		        'fallback_cb'     => 'wp_page_menu',
		        'items_wrap'      => '<ul class="main-menu">%3$s<li class="login">'. $login_logout .'</li></ul>',
		        'depth'           => 0
		      )
		    ); 
	    ?>

	    </div>
        
        <div class="m-menu-btn"> <span> </span> <span> </span> <span> </span> <span> </span> </div>

	    </div>
    </header>
    
    <div class="container">
		<div class="container-contact100">
			<div class="wrap-contact100">

<?php } ?>
