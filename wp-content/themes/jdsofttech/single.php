<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Jdsofttech
 */

if(!is_user_logged_in() && is_singular( 'invoice' ) ){

	global $post;	 
	
	$activate_key = get_post_meta( $post->ID, "single_invoice_activate_key", true);
	
	if(!empty($_GET['key']) && $_GET['key'] == $activate_key ){

	}else{

		wp_redirect(home_url());
 		exit();

	}
}

get_header();
?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			if ( get_post_type( get_the_ID() ) == 'invoice' ) {

				get_template_part( 'page-templates/single-invoice' );

			} else if ( get_post_type( get_the_ID() ) == 'customer' ) {

				get_template_part(  'page-templates/single-customer' );

			}


			
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
