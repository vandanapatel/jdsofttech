<?php
/**
 * Jdsofttech functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Jdsofttech
 */

if ( ! function_exists( 'jdsofttech_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function jdsofttech_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Jdsofttech, use a find and replace
		 * to change 'jdsofttech' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'jdsofttech', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'header-menu' => esc_html__( 'Header Menu', 'jdsofttech' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'jdsofttech_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'jdsofttech_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function jdsofttech_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'jdsofttech_content_width', 640 );
}
add_action( 'after_setup_theme', 'jdsofttech_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function jdsofttech_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'jdsofttech' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'jdsofttech' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'jdsofttech_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function jdsofttech_scripts() {
	wp_enqueue_style( 'jdsofttech-style', get_stylesheet_uri(), array(), null );

	wp_enqueue_script( 'jdsofttech-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'jdsofttech-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_footer', 'jdsofttech_scripts' );

function custom_jdsofttech_scripts() {
	
	// Jdsofttech CSS
	
	
	
	// 
	wp_enqueue_style( 'jdsofttech-fontawesome-css', get_template_directory_uri() . '/assets/css/fontawesome-all.min.css', array(), null );
	wp_enqueue_style( 'jdsofttech-bootstrapmin-css', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), null );
	wp_enqueue_style( 'jdsofttech-dataTables-css', get_template_directory_uri() . '/assets/css/jquery.dataTables.min.css', array(), null );
	wp_enqueue_style( 'jdsofttech-style-css', get_template_directory_uri() . '/assets/css/style.css', array(), null );
	wp_enqueue_style( 'jdsofttech-header-footer', get_template_directory_uri() . '/assets/css/header-footer.css', array(), null );

	if(is_page_template( 'page-templates/tmp-login.php' ) || is_page_template( 'page-templates/tmp-forgot-password.php' )  || is_page_template( 'page-templates/tmp-reset-password.php' )){

		wp_enqueue_style( 'jdsofttech-fontawesome-css', get_template_directory_uri() . '/assets/css/fontawesome-all.min.css', array(), null );
		wp_enqueue_style( 'jdsofttech-material-design-css', get_template_directory_uri() . '/assets/fonts/iconic/css/material-design-iconic-font.min.css', array(), null );
		wp_enqueue_style( 'jdsofttech-loginutil-css', get_template_directory_uri() . '/assets/css/login/util.css', array(), null );
		wp_enqueue_style( 'jdsofttech-login-css', get_template_directory_uri() . '/assets/css/login/login.css', array(), null );
	}

	wp_enqueue_style( 'jdsofttech-responsive-css', get_template_directory_uri() . '/assets/css/responsive.css', array(), null );

	// JS

	wp_enqueue_script('jquery');
	wp_enqueue_script( 'jdsofttech-bootstrapmin-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), null, true );
	wp_enqueue_script( 'jdsofttech-dataTablesmin-js', get_template_directory_uri() . '/assets/js/jquery.dataTables.min.js', array(), null, true );

	wp_enqueue_script( 'jdsofttech-common', get_template_directory_uri() . '/assets/js/common.js', array(), null, true );

}
add_action( 'wp_enqueue_scripts', 'custom_jdsofttech_scripts',1 );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

add_action('customize_register','custom_theme_customizer');
function custom_theme_customizer( $wp_customize){

    //create panel for Mayo Foundationpress
    $wp_customize->add_panel('footer_copyright_panel', array(
        'priority'          => 2002,
        'title'             => __('Footer Options', 'foundationpress'),
        'description'       => __('Options for Theme Footer', 'foundationpress')
    ));

	$wp_customize->add_section('footer_copyright_sec', array(
        'priority'          => 1007,
        'panel'             => 'footer_copyright_panel',
        'title'             => __('Footer Copyright', 'foundationpress'),
        'description'       => __('Edit Footer Copyright Option', 'foundationpress')
    ));
    $wp_customize->add_setting('footer_copyright_text');
    $wp_customize->add_control( 'footer_copyright_text', array(
            'type'              => 'text',
            'section'           => 'footer_copyright_sec', 
            'label'             => __( 'Footer Copyright Text' ),
           ));   
}
/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}



add_action('wp_head', 'myplugin_ajaxurl');

function myplugin_ajaxurl() {

   echo '<script type="text/javascript">
            var ajaxurl = "' . admin_url('admin-ajax.php') . '";
           	var site_url = "'. site_url() .'";
           	var new_customer_url = "'. site_url('/add-new-customer/') .'";
           	var new_invoice_url = "'. site_url('/add-new-invoice/') .'";
           	var edit_invoice_url = "'. site_url('/edit-invoice/') .'";
         </script>';
}

/* Convert number to Words - Start */

$ones = array(
	"",
	" One",
	" Two",
	" Three",
	" Four",
	" Five",
	" Six",
	" Seven",
	" Eight",
	" Nine",
	" Ten",
	" Eleven",
	" Twelve",
	" Thirteen",
	" Fourteen",
	" Fifteen",
	" Sixteen",
	" Seventeen",
	" Eighteen",
	" Nineteen"
);
 
$tens = array(
	"",
	"",
	" Twenty",
	" Thirty",
	" Forty",
	" Fifty",
	" Sixty",
	" Seventy",
	" Eighty",
	" Ninety"
);
 
$triplets = array(
	"",
	" Thousand",
	" Million",
	" Billion",
	" Trillion",
	" Quadrillion",
	" Quintillion",
	" Sextillion",
	" Septillion",
	" Octillion",
	" Nonillion"
);
 
 // recursive fn, converts three digits per pass
function convertTri($num, $tri) {
	global $ones, $tens, $triplets;

	$r = (int) ($num / 1000);
	$x = ($num / 100) % 10;
	$y = $num % 100;

	$str = "";

	if ($x > 0)
		$str = $ones[$x] . " Hundred";

	if ($y < 20)
		$str .= $ones[$y];
	else
		$str .= $tens[(int) ($y / 10)] . $ones[$y % 10];

	if ($str != "")
		$str .= $triplets[$tri];

	if ($r > 0)
		return convertTri($r, $tri+1).$str;
	else
		return $str;
}

function convertNum($num) {
	$num = (int) $num;    

	if ($num < 0)
	return "Negative".convertTri(-$num, 0);

	if ($num == 0)
	return "Zero";

	return convertTri($num, 0);
}
 
/* Convert number to Words - Start */


// Update Title name with Customer name 

add_action('acf/save_post', 'save_post_type_post', 20); // fires after ACF
function save_post_type_post($post_id) {
	$post_type = get_post_type($post_id);
	if ($post_type != 'invoice') {
		return;
	}
	$post_title = get_field('cust_invoice_number', $post_id);
	$post_name = sanitize_title($post_title);
	$post = array(
		'ID' => $post_id,
		'post_name' => $post_name,
		'post_title' => $post_title
	);
	wp_update_post($post);
}


add_action('acf/save_post', 'save_post_type_post1', 20); // fires after ACF
function save_post_type_post1($post_id) {
	$post_type = get_post_type($post_id);
	if ($post_type != 'customer') {
		return;
	}
	$post_title = get_field('customer_name', $post_id);
	$post_name = sanitize_title($post_title);
	$post = array(
		'ID' => $post_id,
		'post_name' => $post_name,
		'post_title' => $post_title
	);
	wp_update_post($post);
}


// Create Backend database for invoice and Customer post type

register_post_type( 'customer',
    array(
        'labels' => array(
                'name' => __( 'Customers' ),
                'singular_name' => __( 'Customer' )
        ),
	    'public' => true,
	    'has_archive' => true,
	    'show_in_menu'        => true,
	    'show_in_nav_menus'   => true,
	    'show_in_admin_bar'   => true,
	    'show_ui'             => true,
	    'menu_name'           => __( 'Customers', 'jdsofttech' ),
	    )
);

register_post_type( 'invoice',
    array(
        'labels' => array(
                'name' => __( 'Invoices' ),
                'singular_name' => __( 'Invoice' )
        ),
	    'public' => true,
	    'has_archive' => true,
	    'show_in_menu'        => true,
	    'show_in_nav_menus'   => true,
	    'show_in_admin_bar'   => true,
	    'show_ui'             => true,
	    'menu_name'           => __( 'Invoices', 'jdsofttech' ),
	    )
);



function my_acf_input_admin_footer() { ?>

	<script type="text/javascript">
		(function($) {

			function calculate(){
				var sum = 0;
			  //	var calculated_total_sum = 0;     
	 			$("tr.acf-row").each(function() {
				    var amount = $(this).find(".invc_amount_cls input[type=number]").val();
				    sum += +amount;
				});
				$(".invc_totalamount_cls input[type=number]").val(sum);
				  
	 		}

			function quant_rate_calculation(){

				$('tr.acf-row').each(function(){					

					var quant_id = $(this).find(".invc_qaunt_cls input").attr("id");
					$('#'+quant_id).change(function(){

						var quant_no = $(this).val();
						var rat_no = $(this).parents().siblings(".invc_rate_cls").find("input" ).val();
						var total = quant_no*rat_no;
						$(this).parents().siblings(".invc_amount_cls").find("input" ).val(total); 
						//alert('qun');
						calculate();
						
					});

					var rate_id = $(this).find(".invc_rate_cls input").attr("id");
					$('#'+rate_id).change(function(){
						
						var rat_no1 = $(this).val();
						var quant_no1 = $(this).parents().siblings(".invc_qaunt_cls").find("input" ).val();
						var total = quant_no1*rat_no1;
						$(this).parents().siblings(".invc_amount_cls").find("input" ).val(total);
						
						calculate();						

					});
				});
			} 		        
				
			acf.add_action('ready', function( $el ){					
				
				jQuery( ".invc_amount_cls input" ).attr("readonly", "readonly");	
				jQuery( ".invc_totalamount_cls input" ).attr("readonly", "readonly");		

				quant_rate_calculation();				
			
			});	
			
			acf.add_action('append', function( $el ){			
								
				quant_rate_calculation();
				
			});				

			// Dates Validation

			var start_date_key = 'field_5ae6d5778d40b'; 
			var end_date_key = 'field_5ae6d5988d40c'; 
			  
			if (typeof(acf) != 'undefined') {

			    acf.add_action('date_picker_init', function($input, args, $field) {
			    
			    	var key = $input.closest('.acf-field').data('key');
			
				    if (key == start_date_key) {
				    	
				        $input.datepicker().on('input change select', function(e) {
				          
					        var date = $(this).datepicker('getDate');
					          
					        date.setDate(date.getDate()+1);
					         
					       $('[data-key="'+end_date_key+'"] input.hasDatepicker').datepicker("option", "minDate", date);
				        });
			      	}			    

			    });

			}


		})(jQuery);	

	</script>
	
	<?php
		
}

add_action('acf/input/admin_footer', 'my_acf_input_admin_footer');

// Get Selected Existing Customer-Id
add_action('wp_ajax_existing_customer', 'existing_customer_ajaxdata');
add_action('wp_ajax_nopriv_existing_customer', 'existing_customer_ajaxdata');
function existing_customer_ajaxdata(  $cust) {

	$cust = $_REQUEST['customer'];
	echo wp_send_json($cust);
	    
}


function acf_load_color_field_choices1( $field ) {    
   
 	
    $field['choices'] = array();

	if(isset($_GET['edit_Cid'] )){
	$cust = $_GET['edit_Cid'];

	$choices[] = get_post_meta($cust, 'customer_address_0_customer_state', true);
	    foreach( $choices as $choice ) {

	    	$field['choices'][ $choice ]= $choice;
	    	
	    }		
	}
    return $field;
    
}

add_filter('acf/load_field/name=customer_state', 'acf_load_color_field_choices1');	

 

add_action( 'parse_request', 'wpse132196_redirect_after_trashing_get' );
function wpse132196_redirect_after_trashing_get() {
    if ( array_key_exists( 'deleted', $_GET ) && $_GET['deleted'] == '1' ) {
        wp_redirect( home_url('/invoices/') );
        exit;
    }
}

// Copy Invoice 

add_action('wp_ajax_invoice_copy', 'invoice_copy_ajaxdata');
add_action('wp_ajax_nopriv_invoice_copy', 'invoice_copy_ajaxdata');

function invoice_copy_ajaxdata(){
	global $wpdb;
	if (! ( isset( $_REQUEST['invoice']) || isset( $_REQUEST['invoice'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}

	$post_id = $_REQUEST['invoice'];
	$post = get_post( $post_id );
 	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	if (isset( $post ) && $post != null) {
 
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'publish',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		$new_post_id = wp_insert_post( $args );
 	
 	 		
 	
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				if( $meta_key == '_wp_old_slug' ) continue;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 		echo wp_send_json($new_post_id);
		wp_redirect( site_url('?duplicate_invoice_id='.$new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}


// Get Countries from custom Table

function acf_load_color_field_choices( $field ) {
    
    $field['choices'] = array();
    global $wpdb;

	$myrows = $wpdb->get_results( "SELECT * FROM wp_countries" );

	    foreach( $myrows as $choice ) {		
	    	    	//echo $choice ;
	    	$field['choices'][ $choice->id ]= $choice->name;
	    	
	    }		

    return $field;
    
}

add_filter('acf/load_field/name=customer_country', 'acf_load_color_field_choices');	

// Get State Based on Country

add_action('wp_ajax_country_state', 'state_ajaxdata');
add_action('wp_ajax_nopriv_country_state', 'state_ajaxdata');

function state_ajaxdata(){

	echo $cust = $_GET['edit_Cid'];

	if(!empty($_REQUEST["country"])) {

		global $wpdb;
		$myrows = $wpdb->get_results( "SELECT * FROM wp_states WHERE country_id = '" . $_REQUEST["country"] . "'" );

		?>
		<option value="">Select State</option>
		<?php
		foreach($myrows as $state) {
		
			if(isset($_GET['edit_Cid'])){
				$cust = $_GET['edit_Cid'];

			echo	$state1= get_post_meta($cust, 'customer_address_0_customer_state', true); ?>

				<option value="<?php echo $state->name; ?>" <?php if( $state->name ==$state1){ ?> selected="selected" <?php } ?>><?php echo $state->name; ?></option>

			<?php
			} else { ?>

				<option value="<?php echo $state->name; ?>"><?php echo $state->name; ?></option>

			<?php

			}
		}
	}

	exit;
}


// Rmeove Archieve page form theme

function wpa_parse_query( $query ){
	global $post;
	//echo  $post->ID;
	
	
	if(is_archive() && !is_admin()) {
	    global $wp_query;
	    $wp_query->set_404();
	    status_header( 404 );
	    nocache_headers();
	    include("404.php");
	    die;
	}
}
add_action( 'parse_query', 'wpa_parse_query' );

// Redirect page only can use amdin user.

add_action( 'template_redirect', 'redirect_non_logged_users_to_specific_page' );
function redirect_non_logged_users_to_specific_page() {	

	if ((!is_user_logged_in() && is_front_page()) || (!is_user_logged_in() && is_page('add-new-customer')) || (!is_user_logged_in() && is_page('add-new-invoice')) || (!is_user_logged_in() && is_page('customers')) || (!is_user_logged_in() && is_page('edit-invoice')) || (!is_user_logged_in() && is_page('add-new-invoice'))  ) {
	
    	wp_redirect( home_url('/log-in/') ); 

	} 
}

//for wp-login.php and wp-admin page redirect to log-in page

add_action( 'init','redirect_login_page' );
function redirect_login_page(){

    global $pagenow;
    $page = site_url('/login/');
	if( 'wp-login.php' == $pagenow ) {

	 	wp_redirect( home_url('/log-in/') );     
 
	}

}

add_filter('login_redirect', 'admin_default_page');
function admin_default_page() {
  return site_url();
}



// After logout Redirect to Site URL

function go_home(){
  wp_redirect( home_url() );
  exit();
}
add_action('wp_logout','go_home');


function get_tiny_url($url)  {  
	$ch = curl_init();  
	$timeout = 5;  
	curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);  
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);  
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);  
	$data = curl_exec($ch);  
	curl_close($ch);  
	return $data;  
}




// Get Selected Existing Customer-Id
add_action('wp_ajax_pdf_invoice', 'pdf_invoice_ajaxdata');
add_action('wp_ajax_nopriv_pdf_invoice', 'pdf_invoice_ajaxdata');
function pdf_invoice_ajaxdata(  $cust) {

	$postid = $_REQUEST['pdf_id'];

	$inoivce_no = get_field('cust_invoice_number',$postid);
	$customer = get_field('cust_invoice_customer', $postid);
	$invoice_date = get_field('cust_invoice_invdate', $postid);
	$due_date = get_field('cust_invoice_duedate', $postid);		

    if( have_rows('customer_address', $customer->ID) ): 
			while ( have_rows('customer_address', $customer->ID) ) : the_row();
        	$street = get_sub_field('customer_street', $customer->ID);
        	$zipcode = get_sub_field('customer_zipcode', $customer->ID);
        	$state = get_sub_field('customer_state', $customer->ID);
        	$city = get_sub_field('customer_city', $customer->ID);
     
        	$otherinfo = '<div class="customer"> <span> Customer: </span> <a href="'.get_permalink($customer->ID).'">'. get_field('customer_name', $customer->ID) .'</a><p>' .$street. '</br>'.$zipcode . ' ' . $state . '</br>Ph : +'. get_field('customer_phone_no', $customer->ID).  '</p>' ;		             	 	
  	 	endwhile;
    endif;

 	global $tfoot, $tbody, $tcontent, $total, $tbodyend;	 
 	$tcontent = '';
   	if( have_rows('cust_invoice_website',$postid) ):
        $tbody = '<tbody>';
        	while ( have_rows('cust_invoice_website',$postid) ) : the_row();
		        $tcontent .= '<tr>
		        	<td class="col-description">'. get_sub_field('cust_invoice_desc',$postid) .'</td>
		        	<td class="col-description">'. get_sub_field('cust_invoice_quantity',$postid) .'</td>
		        	<td class="col-description">'. get_sub_field('cust_invoice',$postid) .'</td>
		        	<td class="col-amount"> $ '. get_sub_field('cust_invoice_amount',$postid) .'</td>
		        	</tr>
		        	<tr style="background: #faf9fc;">
		        	<td class="col-description"></td>
		        	<td class="col-description"></td>
		        	<td class="col-description"> </td>
		        	<td class="col-amount"></td>
		        	</tr>';
			
			$total += intval( get_sub_field('cust_invoice_amount',$postid));
			endwhile;
		$tbodyend ='</tbody>
			<tfoot>
	          <tr>
	            <td  colspan="3"><div class="total">Total</div></td>
	            <td><div class="total amount"> $'. $total .'</div></td>
	          </tr>
	        </tfoot>';	              	
  	endif;
  		//$html = '<style>'.file_get_contents(WP_CONTENT_DIR . '/themes/jdsofttech/assets/css/style.css').'</style>';
		     $logo = 'http://localhost/jdsofttech/wp-content/themes/jdsofttech/assets/img/logo.png';

	$html = '<style>
			@page {
			   size: 15in 19.55in;
			   border: 1px solid #bdbec1;
			   margin: 0; 
			}
			@media print {
				@charset "utf-8";				
				html, body {
					background: #fff;
					border: 0;
					padding: 10mm 8mm;
					margin:0;					   
				}
				.single_invoice_div a, .single_invoice_div a:hover, .single_invoice_div a:focus {
					text-decoration: none;
					outline: none;
				}
				.single_invoice_div span {
					text-transform: uppercase;
					color: #ccc;
					font-size: 20px;
					display: block;
				}		
				.top-heading {
					background: url('.get_template_directory_uri().'/assets/img/ribbon.png) no-repeat center center;
					width: 619px;
					height: 75px;
					background-size: cover;
					text-align: center;
					margin: -37px auto 0;
				}
				.top-heading h1 {
					color: #fff;
					margin: 0;
					line-height: 70px;
					font-size: 36px;
				}
				.supplier-customer-box-row table {
				    width: 100%;
				    border-top: 1px solid #d1d2d4;
				    margin-bottom: 50px;	
				}
				.supplier-customer-box-row .left-colum  {
				    width: 50%;
				    border-right: 1px solid #d1d2d4;
				    padding: 0px 0px 0px 15px;
				    vertical-align: middle;
				}
				.right-culum {
					width: 50%;
					vertical-align: top;
					padding: 0px;
				}

				.supplier {
					padding: 50px 0px 15px 0px;
				}

				.address {
					padding: 15px 0px 15px 0px;
					margin-bottom: 70px;
				}
				.supplier-customer-box-row span {
					text-transform: uppercase;
					color: #818285;
					font-size: 20px;
					display: block;
					margin-bottom: 10px;
				}
				.supplier-customer-box-row p {
					font-size: 24px;
					color: #333;
					margin-bottom: 10px;
					line-height: 40px;
				}
				.supplier a {
					color: #2185c7;
					font-size: 30px;
					display: block;
					text-transform: uppercase;
					margin-bottom: 10px;
				}
				.address ul {
					list-style: none;
					padding: 0;
				}
				.address ul li {
					display: block;
				}
				.address ul li span, .address ul li a {
					display: inline-block;
				}
				.address ul li a {
					font-size: 24px;
					color: #2186c8;
				}
				.foot-left-col {
					background: #2186c8;
					padding: 15px 30px;
					font-size: 30px;
					color: #fff;
					border-right: 1px solid #ffffff;
				}
				.foot-right-col {
					background: #e7e7e8;
					padding: 15px 30px;
					font-size: 30px;
					color: #818285;
					text-align: right;
				}
				.customer {
					background: #e7e7e8;
					border: 8px solid #bcbdc0;
					padding: 50px 0px 70px 40px;
					max-height: 500px;
					position: relative;
				}
				.customer:before {
					position: absolute;
					left: -94px;
					top: -8px;
					width: 94px;
					height: 86px;
					content: url('.get_template_directory_uri().'/assets/img/customer-ribon.png);
				}
				.customer a {
					color: #000;
					font-size: 30px;
					display: block;
					margin-bottom: 10px;
				}
				.description-amount-table-row table {
					width: 100%;
					margin-bottom: 50px;
					width: 100%;
				}
				.description-amount-table-row table thead {
					background-color: #231f20;
					color: #fff;
					text-transform: uppercase;
					font-size: 20px;
					font-weight: normal;
				}
				.description-amount-table-row table th, .description-amount-table-row table td {
					padding: 15px;
					line-height: 20px;
					text-align: left;
					vertical-align: top;
				}
				.description-amount-table-row .col-description {
					border-right: 4px solid #fff;
				}
				.description-amount-table-row td.col-description {
					color: #000;
					font-size: 18px;
				}
				.description-amount-table-row .col-amount {
					text-align: right;
				}
				.description-amount-table-row td.col-amount {
					color: #2186c8;
					font-size: 18px;
				}
				.description-amount-table-row > table > tfoot tr td {
					padding: 0;
				}
				.description-amount-table-row .total {
					background-color: #2186c8;
					color: #fff;
					padding: 20px 15px;
					min-width: 150px;
					text-transform: uppercase;
					font-size: 20px;
					float: right;
					text-align: center;
				}
				.description-amount-table-row .amount {
					width: 100%;
					border-left: 2px solid #fff;
					text-align: right;
				}
				.contenteditable {
					padding: 50px 0px;
				}
				.contenteditable p {
					font-size: 20px;
					color: #818285;
					margin: 0px;
					line-height: 30px;
				}
				.thank-you-text {
					padding: 25px 0px;
					text-align: center;
					font-size: 30px;
					color: #818285;
					border-top: 1px solid #d1d2d4;
				}

			}
	</style>
				<section class="content-section single_invoice_div">
				  	
				 	<div class="top-heading">
				    	<h1> Invoice number: '.$inoivce_no.'</h1>
				  	</div>
				  	<div class="logo-block-row1" style="position: relative;text-align: center;margin: 10px 0px;">  
				  		<img src="'.get_template_directory_uri().'/assets/img/logo-block-row-line.png" alt="JDsofttech" style="top: 50%;left: 0; width: 100%; height: 16px; margin-top: 80px; background-size: cover;">
				  		<div class="logo1" style="width:300px;margin:0 auto 50px; text-align:center;">
				  			<img src="'.get_template_directory_uri().'/assets/img/logo.png" alt="JDsofttech" style="width: 280px;margin: -100px auto 0;display: block;height: auto;"/>
				  		</div>				  		
				 	</div>	
				 	
				  	<div class="supplier-customer-box-row">
					    <div class="table-responsive-md">
						 	<table width="0" border="0" cellpadding="0" cellspacing="0">							
								<tr>											
						            <td class="left-colum">
						            	<img src>
						            	<div class="supplier"> <span> supplier </span> <a href="'. site_url() .'"> JD Softtech INC . </a>
						                	<p> Web & Software Developments Company </p>
						              	</div>
						              	<div class="address"> <span> Address </span>
							                <p> 903 Main Street, Hanson,</br>
							                  02341,MA,</br>
							                  USA </p>
							                <ul>
							                  <li> <span> Phone: </span> <a href="#">
							                    <p>+1 (781) 9744568 </p>
							                    </a> </li>
							                  <li> <span> Website: </span> <a href="https://www.jdsofttech.com/"> www.jdsofttech.com </a> </li>
							                  <li> <span> E-mail: </span> <a href="mailto:info@jdsofttech.com"> info@jdsofttech.com </a> </li>
							                </ul>
							             </div>
						          	</td>		
						          						              							
									<td class="right-culum">'.$otherinfo .'</td> 
								</tr>
								<tfoot>
							        <tr>							          		
						               <td class="left-colum foot-left-col"> Date :'. $invoice_date .' </td>
						               <td class="right-colum foot-right-col"> Date : '.  $due_date.' </td>
						            </tr>
						        </tfoot>	
							</table>
						</div>
			 		</div>

			 		<div class="description-amount-table-row">
					    <div class="table-responsive-sm">
					    	<table class="table-striped">
								<thead>
								   <tr>
							            <th class="col-description"> Description </th>
							            <th class="col-description"> Quantity </th>
							            <th class="col-description"> Rate </th>
							            <th class="col-amount"> Amount </th>
						          	</tr> 
						        </thead>'
						        . $tbody . $tcontent . $tbodyend .
						    '</table>
					    </div>
				  	</div>
			 		<div class="contenteditable">  		
					    <p> Amount in words:'. convertNum(  $total ) .' Dolloars Only. </p>
					    <p> Declaration: We declare that this invoice shows the actual price for LeadersUp and all particulars are true and  correct. </p>
				  	</div>
				  	<div class="thank-you-text"> Thank you for your business! </div>
				
			</section>';
	echo wp_send_json($html);
	    
}