
jQuery(document).ready(function(){

	hide_customer_fields();
	hide_selected_customer_fields();


	// Calculation with Quantity and Rate with Repeader Values 
	jQuery( ".csm_amount input" ).attr("readonly", "readonly");		

	jQuery( ".csm_quantity:first input" ).on('change',function() {
		var quant = jQuery( this ).val();				
		var rat = jQuery( ".csm_rate input" ).val();			
		var total_amount = quant*rat;
		jQuery(".csm_amount:first input" ).val(total_amount);
	});

	jQuery( ".csm_rate:first input" ).on('change',function() {
		var quant = jQuery(".csm_quantity input" ).val();				
		var rat = jQuery( this ).val();
		var total_amount = quant*rat;
		jQuery( ".csm_amount:first input" ).val(total_amount);
	});

	// Hide invoice Fields
	function hide_invoice_fields(){
		jQuery("#csm_customer").hide();
		jQuery("#csm_select_customer").hide();
	}

	// Hide select customer Fields
	function hide_selected_customer_fields(){
		jQuery("#csm_customer").hide();
		jQuery("#csm_select_customer").hide();
	}

	// Show select customer Fields
	function show_selected_customer_fields(){
		jQuery("#csm_customer").show();
		jQuery("#csm_select_customer").show();
	}

	// Hide Customer Fields
	function hide_customer_fields(){
		jQuery(".acf-form-submit input").hide();
		jQuery("#csm_invoice_number").hide();
		jQuery("#csm_customer_name").hide();
		jQuery("#csm_address").hide();
		jQuery("#csm_phone_number").hide();
		jQuery("#csm_invoice_date").hide();
		jQuery("#csm_due_date").hide();
		jQuery("#csm_website").hide();	
		jQuery("#csm_customer_email").hide();	
		
	}

	// Show Customer Fields
	function show_customer_fields(){			
		jQuery(".acf-form-submit input").show();
		jQuery("#csm_invoice_number").show();
		jQuery("#csm_customer_name").show();
		jQuery("#csm_address").show();
		jQuery("#csm_phone_number").show();
		jQuery("#csm_invoice_date").show();
		jQuery("#csm_due_date").show();
		jQuery("#csm_website").show();	
		jQuery("#csm_customer_email").show();	
	}

	// Select Invoice
	jQuery("#csm_invoice_copynew input").on('change', function() {
	    var customer_val = jQuery(this).val();
	   // alert(customer_val);
	   	if(customer_val == 'copy_invoice'){	
	   		jQuery("#csm_select_invoice").show();
			jQuery("#csm_customer").hide();
			jQuery("#csm_select_customer").hide();					
			hide_customer_fields();

	   	}else if(customer_val == 'new_invoice'){
	   		jQuery("#csm_customer").show();
	   		jQuery("#csm_select_invoice").hide();
			jQuery("#csm_select_customer").show();
	   	}
	   	
	});
	// Select Custom type
	jQuery("#csm_customer input").on('change', function() {
	    var customer_val = jQuery(this).val();
	   // alert(customer_val);
	   	if(customer_val == 'existing'){
	   		jQuery("#csm_select_customer").show();
			hide_customer_fields();	
	   	}else if(customer_val == 'new'){
	   		jQuery("#csm_select_customer").hide();
			show_customer_fields();
	   	}
	   	
	});

	// Select Invoice AJAX		
    jQuery("#csm_select_invoice select").on('change',function(e){

	//	var ajaxurl= '<?php echo admin_url('admin-ajax.php') ; ?>';
		var exe_cust = jQuery( '#csm_select_invoice option:selected' ).val();
		
		
        jQuery.ajax({
              	url: ajaxurl,
              	async: true,
              	data: {action  : 'invoice_copy',customer: exe_cust},
              	success: function(data){
              		//alert(data);
          			window.location.href = site_url+'?duplicate_invoice_id='+data;
					 
              	}
        });
       
	}); 

	// Select Customer AJAX		
    jQuery("#csm_select_customer select").on('change',function(e){

		
		var exe_cust = jQuery( '#csm_select_customer option:selected' ).val();
		
		
        jQuery.ajax({
              	url: ajaxurl,
              	async: true,
              	data: {action  : 'existing_customer',customer: exe_cust},
              	success: function(data){
              		
				    window.location.href = site_url+'?invoice_id='+data;

              	}
        });
       
	}); 

	// Check Query String
	
	function getUrlVars(){
	    var vars = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	        hash = hashes[i].split('=');
	        vars.push(hash[0]);
	        vars[hash[0]] = hash[1];
	    }
	    return vars;
	}

	var invoice_id = getUrlVars()["invoice_id"];
	var duplicate_invoice_id = getUrlVars()["duplicate_invoice_id"];

	if (jQuery.isNumeric(invoice_id) || jQuery.isNumeric(duplicate_invoice_id) ){

		jQuery("#csm_customer input[value='existing']").attr('checked','checked');
    	jQuery("#csm_customer input[value='new']").removeAttr('checked','checked');	
    	jQuery("#csm_invoice_copynew input[value='new_invoice']").attr('checked','checked');	
    	jQuery("#csm_invoice_copynew input[value='copy_invoice']").removeAttr('checked','checked');	
    	show_customer_fields();
    	show_selected_customer_fields();
		
		// Date Validation in update Customer	    	
    	var invoicedate = jQuery('[data-key="field_5acb0bdfbc9ec"] input.hasDatepicker').datepicker('getDate');
		invoicedate.setDate(invoicedate.getDate()+1);					       
		jQuery('[data-key="field_5acb0c00bc9ed"] input.hasDatepicker').datepicker("option", "minDate", invoicedate);	
	
	}	  

	// Invoice Listing table

	jQuery('#table-invoicelist').DataTable({
		//"pageLength": 5
	});


	// get state
	/*var xmlhttp;



	function showState(str)
	{
	xmlhttp=GetXmlHttpObject();

	if (xmlhttp==null)
	  {
	  alert ("Browser does not support HTTP Request");
	  return;
	  }
	var url=ajaxurl;
	url=url+"?country="+str;
	url=url+"&sid="+Math.random();
	xmlhttp.onreadystatechange=stateChanged;
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
	}

	function stateChanged()
	{
	if (xmlhttp.readyState==1)
	{
	document.getElementById("getStates").innerHTML= "<option selected=\"selected\">...Searching....</option><img src=\"images/spinner.gif\" />";
	}
	if (xmlhttp.readyState==4)
	{
	document.getElementById("getStates").innerHTML=xmlhttp.responseText;
	}
	}

	function GetXmlHttpObject()
	{
	if (window.XMLHttpRequest)
	  {
	  // code for IE7+, Firefox, Chrome, Opera, Safari
	  return new XMLHttpRequest();
	  }
	if (window.ActiveXObject)
	  {
	  // code for IE6, IE5
	  return new ActiveXObject("Microsoft.XMLHTTP");
	  }
	return null;
	}
	*/



	//jQuery('#acf-field_5acaf8e18a012-0-field_5acaf91e8a015').html( data );

    jQuery('#csm_country select').on('change', function() {
    	alert("hii");
        var val = jQuery(this).val();
        //if (val == 223 || val == 224) {
            
            jQuery.ajax({
               url: ajaxurl,
               dataType: 'html',
               data: {action  : 'country_state',country: val},
               success: function(data) {
                   jQuery('#csm_state select').html( data );
               }
            });
       // }
      /*  else {
           jQuery('#state').val('').hide();
           jQuery('#othstate').show();
        }*/
    });

});