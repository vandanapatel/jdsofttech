
jQuery(document).ready(function(){




	// ----- For Invoice -----//
	// Calculation with Quantity and Rate with Repeader Values 
	jQuery( ".invc_amount_cls input" ).attr("readonly", "readonly");		

	jQuery( ".invc_qaunt_cls:first input" ).on('change',function() {
		var quant = jQuery( this ).val();				
		var rat = jQuery( ".invc_rate_cls input" ).val();			
		var total_amount = quant*rat;
		jQuery(".invc_amount_cls:first input" ).val(total_amount);
	});

	jQuery( ".invc_rate_cls:first input" ).on('change',function() {
		var quant = jQuery(".invc_qaunt_cls input" ).val();				
		var rat = jQuery( this ).val();
		var total_amount = quant*rat;
		jQuery( ".invc_amount_cls:first input" ).val(total_amount);
	});

	// Hide invoice Fields
	function hide_invoice_fields(){
		jQuery("#invc_number_cls").hide();
		jQuery("#invc_select_custo_cls").hide();
		jQuery("#invc_invdate_cls").hide();
		jQuery("#invc_duedate_cls").hide();
		jQuery("#invc_website_cls").hide();
		jQuery(".acf-form-submit input").hide();
	}

	// Show invoice Fields
	function show_invoice_fields(){
		jQuery("#invc_number_cls").show();
		jQuery("#invc_select_custo_cls").show();
		jQuery("#invc_invdate_cls").show();
		jQuery("#invc_duedate_cls").show();
		jQuery("#invc_website_cls").show();
		jQuery(".acf-form-submit input").show();
	}

	// Select Invoice
	jQuery("#invc_cpynw_cls input").on('change', function() {

	    var customer_val = jQuery(this).val();
	   // alert(customer_val);
	   	if(customer_val == 'copy_invoice'){	
	   		jQuery("#invc_select_cls").show();							
			hide_invoice_fields();

	   	}else if(customer_val == 'new_invoice'){
	   		jQuery("#invc_select_cls").hide();
	   		show_invoice_fields();
	   	}
	   	
	});

	// Select Invoice AJAX		
    jQuery("#invc_select_cls select").on('change',function(e){

	//	var ajaxurl= '<?php echo admin_url('admin-ajax.php') ; ?>';
		var exe_inv = jQuery( '#invc_select_cls option:selected' ).val();
		
		
        jQuery.ajax({
              	url: ajaxurl,
              	async: true,
              	data: {action  : 'invoice_copy',invoice: exe_inv},
              	success: function(data){
              		//alert(data);
          			window.location.href = new_invoice+'?duplicate_invoice_id='+data;
					 
              	}
        });
       
	}); 


	// ---- For Customer   ------ //

	// Hide Customer Fields
	function hide_customer_fields(){
		jQuery(".acf-form-submit input").hide();
		jQuery("#cust_name_cls").hide();
		jQuery("#cust_email_cls").hide();
		jQuery("#cust_phoneno_cls").hide();
		jQuery("#cust_address_cls").hide();
		jQuery("#cust_selexis_cls").show();	
		
	}

	// Show Customer Fields
	function show_customer_fields(){			
		jQuery(".acf-form-submit input").show();
		jQuery("#cust_name_cls").show();
		jQuery("#cust_email_cls").show();
		jQuery("#cust_phoneno_cls").show();
		jQuery("#cust_address_cls").show();
		jQuery("#cust_selexis_cls").hide();	
	}

	// Get state
    jQuery('#cust_country_cls select').on('change', function() {
    	//alert("hii");
        var val = jQuery(this).val();
         
        jQuery.ajax({
           url: ajaxurl,
           dataType: 'html',
           data: {action  : 'country_state',country: val},
           success: function(data) {
               jQuery('#cust_state_cls select').html( data );
           }
        });

    });

	jQuery('#cust_state_cls select').on('change', function () {
       var value = jQuery(this).val();
       jQuery(this).find('option').not(this).removeAttr("selected", "selected");
       jQuery(this).find('option[value="' + value + '"]').attr("selected", "selected");

       jQuery(this).find('option').not(this).removeAttr("data-i", "0");
       jQuery(this).find('option[value="' + value + '"]').attr("data-i", "0");
       
    });

	// Select Custom type
	jQuery("#cust_selcustomer_cls input").on('change', function() {
	    var customer_val = jQuery(this).val();
	   // alert(customer_val);
	   	if(customer_val == 'existing'){
	   		jQuery("#cust_selexis_cls").show();
			hide_customer_fields();	
	   	}else if(customer_val == 'new'){
	   		jQuery("#cust_selexis_cls").hide();
			show_customer_fields();
	   	}
	   	
	});

	// Select Customer AJAX		
    jQuery("#cust_selexis_cls select").on('change',function(e){

		
		var exe_cust = jQuery( '#cust_selexis_cls option:selected' ).val();
		alert(exe_cust);
		
        jQuery.ajax({
              	url: ajaxurl,
              	async: true,
              	data: {action  : 'existing_customer',customer: exe_cust},
              	success: function(data){
              			
				    window.location.href = new_customer+'?customer_id='+data;

              	}
        });
       
	}); 

	// Check Query String
	
	function getUrlVars(){
	    var vars = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	        hash = hashes[i].split('=');
	        vars.push(hash[0]);
	        vars[hash[0]] = hash[1];
	    }
	    return vars;
	}

	var customer_id = getUrlVars()["customer_id"];
	var duplicate_invoice_id = getUrlVars()["duplicate_invoice_id"];
	//alert(duplicate_invoice_id);
	if (jQuery.isNumeric(customer_id) ){

		jQuery("#cust_selcustomer_cls input[value='existing']").attr('checked','checked');
    	jQuery("#cust_selcustomer_cls input[value='new']").removeAttr('checked','checked');	
    	show_customer_fields();

    } else if(jQuery.isNumeric(duplicate_invoice_id) ) {	

    	jQuery("#invc_cpynw_cls input[value='new_invoice']").removeAttr('checked','checked');	
    	jQuery("#invc_cpynw_cls input[value='copy_invoice']").attr('checked','checked');    	
    	
		// Date Validation in update Customer	    	
    	var invoicedate = jQuery('#invc_invdate_cls input.hasDatepicker').datepicker('getDate');
		invoicedate.setDate(invoicedate.getDate()+1);					       
		jQuery('#invc_duedate_cls input.hasDatepicker').datepicker("option", "minDate", invoicedate);
    
    } else {
		hide_customer_fields();
		hide_invoice_fields();

	}

	// Invoice Listing table

	jQuery('#table-invoicelist').DataTable({
		//"pageLength": 5
	});



});