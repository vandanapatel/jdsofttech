jQuery(document).ready(function(){
	
    jQuery( "#invc_number_cls" ).wrap( '<div class="row invc_number_cls"><div class="col-sm-6 invc_number"><div class="form-group"> </div></div</div>' );
	jQuery( "#invc_select_custo_cls" ).wrap( '<div class="col-sm-6 invc_select_custo"><div class="form-group"> </div></div>' );
	jQuery('.invc_select_custo').insertAfter( '.invc_number' );

	jQuery( "#invc_invdate_cls" ).wrap( '<div class="row invc_invdate_cls"><div class="col-sm-6 invc_invdate"><div class="form-group"> </div></div</div>' );
	jQuery( "#invc_duedate_cls" ).wrap( '<div class="col-sm-6 invc_duedate"><div class="form-group"> </div></div>' );
	jQuery('.invc_duedate').insertAfter( '.invc_invdate' );

	jQuery( "#invc_totalamount_cls" ).wrap( '<div class="row invc_totalamount_cls"><div class="col-sm-6 pull-right"><div class="form-group"> </div></div</div>' );

	jQuery( "#cust_email_cls" ).wrap( '<div class="row"><div class="col-sm-6 cust_email"><div class="form-group"> </div></div</div>' );
	jQuery( "#cust_phoneno_cls" ).wrap( '<div class="col-sm-6 cust_phoneno"><div class="form-group"> </div></div>' );
	jQuery('.cust_phoneno').insertAfter( '.cust_email' );


	jQuery( "#table-invoicelist" ).wrap( '<div class="custom_table_wrapper"></div>' );
	
    jQuery(".m-menu-btn").click(function() {
		jQuery(".menu > ul").slideToggle('slow');
		jQuery(this).toggleClass("m-menu-btn-ext");
	});

	jQuery( '<div class="custom_table_wrapper"></div>' ).insertAfter( '#table-invoicelist_filter' );

});