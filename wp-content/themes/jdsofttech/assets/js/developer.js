jQuery(window).load(function(){

	// var $radios = jQuery('input:radio[name=radio_invoice_email]');
	// alert($radios);
 //    if($radios.is(':checked') === false) {
 //        $radios.filter('[value=radio_customer_email]').prop('checked', true);
 //    }


}); 

jQuery(document).ready(function(){
jQuery(".invc_amount_cls input").addClass('quant_rate_amount');
jQuery(".invc_totalamount_cls input").addClass('total_amount');

	// Hide invoice Fields
	function hide_invoice_fields(){
		jQuery("#invc_number_cls").hide();
		jQuery("#invc_select_custo_cls").hide();
		jQuery("#invc_invdate_cls").hide();
		jQuery("#invc_duedate_cls").hide();
		jQuery(".invc_website_cls").hide();
		jQuery(".invc_totalamount_cls").hide();
		jQuery(".page-template-tmp-invoice-form .acf-form-submit input").hide();
	}

	// Show invoice Fields
	function show_invoice_fields(){
		jQuery("#invc_number_cls").show();
		jQuery("#invc_select_custo_cls").show();
		jQuery("#invc_invdate_cls").show();
		jQuery("#invc_duedate_cls").show();
		jQuery(".invc_website_cls").show();
		jQuery(".invc_totalamount_cls").show();
		jQuery(".page-template-tmp-invoice-form .acf-form-submit input").show();
	}

	//edit Invoice

	function edit_invoice_fields(){
		jQuery(".invc_cpynw_cls").hide();
		jQuery(".invc_select_cls").hide();
		jQuery(".invc_number_cls").show();
		jQuery(".invc_select_custo_cls").show();
		jQuery(".invc_invdate_cls").show();
		jQuery(".invc_duedate_cls").show();
		jQuery(".invc_website_cls").show();
		jQuery(".invc_totalamount_cls").show();
		jQuery(".page-template-tmp-invoice-form .acf-form-submit input").show();
	}

	// Select Invoice
	jQuery("#invc_cpynw_cls input").on('change', function() {

	    var customer_val = jQuery(this).val();
	 //  alert(customer_val);
	   	if(customer_val == 'copy_invoice'){	
	   		jQuery("#invc_select_cls").show();							
			hide_invoice_fields();

	   	}else if(customer_val == 'new_invoice'){
	   		jQuery("#invc_select_cls").hide();
	   		show_invoice_fields();
	   	}
	   	
	});

	// Select Invoice AJAX		
    jQuery("#invc_select_cls select").on('change',function(e){

	//	var ajaxurl= '<?php echo admin_url('admin-ajax.php') ; ?>';
		var exe_inv = jQuery( '#invc_select_cls option:selected' ).val();
		
		
        jQuery.ajax({
              	url: ajaxurl,
              	async: true,
              	data: {action  : 'invoice_copy',invoice: exe_inv},
              	success: function(data){
              		//alert(data);
          			window.location.href = new_invoice_url+'?duplicate_invoice_id='+data;
					 
              	}
        });
       
	}); 

                        
	// ---- For Customer   ------ //

	// Hide Customer Fields
	function hide_customer_fields(){
		jQuery(".page-template-tmp-invoice-form .acf-form-submit input").hide();
		jQuery("#cust_name_cls").hide();
		jQuery("#cust_fname_cls").hide();
		jQuery("#cust_lname_cls").hide();
		jQuery("#cust_email_cls").hide();
		jQuery("#cust_phoneno_cls").hide();
		jQuery("#cust_address_cls").hide();
		jQuery("#cust_selexis_cls").show();	
	
		
	}

	// Show Customer Fields
	function show_customer_fields(){			
		jQuery(".page-template-tmp-invoice-form .acf-form-submit input").show();
		jQuery("#cust_name_cls").show();
		jQuery("#cust_fname_cls").show();
		jQuery("#cust_lname_cls").show();
		jQuery("#cust_email_cls").show();
		jQuery("#cust_phoneno_cls").show();
		jQuery("#cust_address_cls").show();
		jQuery("#cust_selexis_cls").hide();	
	}



	// jQuery('#cust_state_cls select').on('change', function () {
 //       var value = jQuery(this).val();
 //       jQuery(this).find('option').not(this).removeAttr("selected", "selected");
 //       jQuery(this).find('option[value="' + value + '"]').attr("selected", "selected");

 //       jQuery(this).find('option').not(this).removeAttr("data-i", "0");
 //       jQuery(this).find('option[value="' + value + '"]').attr("data-i", "0");
       
 //    });


	// Check Query String
	
	function getUrlVars(){
	    var vars = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	        hash = hashes[i].split('=');
	        vars.push(hash[0]);
	        vars[hash[0]] = hash[1];
	    }
	    return vars;
	}

//	var customer_id = getUrlVars()["customer_id"];
	var duplicate_invoice_id = getUrlVars()["duplicate_invoice_id"];
	var edit_invoice = getUrlVars()["edit_Iid"];

    if(jQuery.isNumeric(duplicate_invoice_id) ) {	

    	jQuery("#invc_cpynw_cls input[value='new_invoice']").attr('checked','checked');	
    	jQuery("#invc_cpynw_cls input[value='copy_invoice']").removeAttr('checked','checked');    	
    	
		// Date Validation in update Customer	    	
    	var invoicedate = jQuery('#invc_invdate_cls input.hasDatepicker').datepicker('getDate');
		invoicedate.setDate(invoicedate.getDate()+1);					       
		jQuery('#invc_duedate_cls input.hasDatepicker').datepicker("option", "minDate", invoicedate);
    
    }else if(jQuery.isNumeric(edit_invoice)){

		jQuery("#invc_cpynw_cls").hide();	
    	jQuery("#invc_select_cls").hide();

    	// Date Validation in update Customer	    	
    	var invoicedate = jQuery('#invc_invdate_cls input.hasDatepicker').datepicker('getDate');
		invoicedate.setDate(invoicedate.getDate()+1);					       
		jQuery('#invc_duedate_cls input.hasDatepicker').datepicker("option", "minDate", invoicedate);

	}else {

		hide_invoice_fields();

	}

	// Invoice Listing table
	jQuery('#table-invoicelist').DataTable({
		"columnDefs": [ {
	          "targets": 'no-sort',
	          "orderable": false,
	    } ]
	});


	var $modal = jQuery('#modal');
	var $modal_target = jQuery('#modal_target');

	jQuery(".view_invoice").each(function(i){

		var invoiceid = jQuery(this).data("viewid");
        jQuery(this).click(function(){
          	jQuery.ajax({
	              	url: ajaxurl,
	              	async: true,
	              	data: {action  : 'view_invoice',viewinvoiceid: invoiceid},
	              	success: function(data){
	              		// alert(data);
	              		console.log(data);
	              		$modal_target.html(data);
     					$modal.modal('show'); 
	          			 
	              	}
	        });
        });
    });


	jQuery(".custom-email").hide();

    jQuery(".mail_invoice").each(function(i){

		var invoiceid = jQuery(this).data("mailinvoiceid");
        jQuery(this).click(function(){
          	jQuery.ajax({
	              	url: ajaxurl,
	              	async: true,
	              	data: {action  : 'mail_invoice',mailinvoiceid: invoiceid},
	              	success: function(data){
	              		$modal_target.html(data);
     					$modal.modal('show'); 
							
							jQuery("input[name=radio_invoice_email]").on('change', function(){
							var radio_mail = jQuery(this).val();
                            if(radio_mail == 'radio_customer_email'){

                              jQuery(".custom-email").hide();
                              jQuery(".customer-email").show();

                            }else if(radio_mail == 'radio_custom_email'){

                              jQuery(".customer-email").hide();
                              jQuery(".custom-email").show();
                            
                            }

                        }); 
	          		 
	              	}
	        });
        });

    });

 	// Change Manual Status to Paid
	jQuery('.manual_autoinv_status').on('change', function() {

	    if(this.checked) {
	    	var current = jQuery(this);	
	    	var invoiceid = jQuery(this).data("status");
	        var status = jQuery(this).val(); 
          	jQuery.ajax({
	              	url: ajaxurl,
	              	async: true,
	              	data: {action  : 'invoice_status',invoice_id: invoiceid, status: status },
	              	success: function(data){ 
	            		current.parent().siblings("span").text(status);  
						current.closest('.markas_paid_check').remove(); 		          		 
	              	}
	        });	            
		}
	});      
	
});