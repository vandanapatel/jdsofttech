<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Jdsofttech
 */

?>
<?php if(get_post_type( get_the_ID() ) != 'invoice' && !is_page_template( 'page-templates/tmp-login.php' ) && !is_page_template( 'page-templates/tmp-forgot-password.php' ) && !is_page_template( 'page-templates/tmp-reset-password.php' )){  ?>
	
			</div>
		</div>
	</div>

	<footer class="footer"> 
	    <div class="container"> 
	    	<p><?php echo get_theme_mod( 'footer_copyright_text' ); ?></p>
	    </div> 
    </footer> 

<?php } ?>



<!-- <script type="text/javascript" src="<?php // echo get_template_directory_uri(); ?>/assets/js/common.js"></script> -->

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/developer.js"></script>

<!-- <script type="text/javascript" src="<?php // echo get_template_directory_uri(); ?>/assets/js/jquery-1.12.4.js"></script> -->

<script type="text/javascript">
	jQuery(window).on("beforeunload", function() { 
	
    return inFormOrLink ? "Do you really want to close?" : null; 
});
</script>

<?php wp_footer(); ?>

</body>
</html>
