<?php
/**
 * Jdsofttech functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Jdsofttech
 */

if ( ! function_exists( 'jdsofttech_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function jdsofttech_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Jdsofttech, use a find and replace
		 * to change 'jdsofttech' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'jdsofttech', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'header-menu' => esc_html__( 'Header Menu', 'jdsofttech' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'jdsofttech_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'jdsofttech_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function jdsofttech_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'jdsofttech_content_width', 640 );
}
add_action( 'after_setup_theme', 'jdsofttech_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function jdsofttech_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'jdsofttech' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'jdsofttech' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'jdsofttech_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function jdsofttech_scripts() {
	wp_enqueue_style( 'jdsofttech-style', get_stylesheet_uri(), array(), null );

	wp_enqueue_script( 'jdsofttech-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'jdsofttech-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'jdsofttech_scripts' );

function custom_jdsofttech_scripts() {
	
	// Jdsofttech CSS
	
	wp_enqueue_style( 'jdsofttech-responsive-css', get_template_directory_uri() . '/assets/css/responsive.css', array(), null );
	wp_enqueue_style( 'jdsofttech-fontawesome-css', get_template_directory_uri() . '/assets/css/fontawesome-all.min.css', array(), null );
	wp_enqueue_style( 'jdsofttech-bootstrapmin-css', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), null );
	wp_enqueue_style( 'jdsofttech-style-css', get_template_directory_uri() . '/assets/css/style.css', array(), null );
	wp_enqueue_style( 'jdsofttech-dataTables-css', get_template_directory_uri() . '/assets/css/jquery.dataTables.min.css', array(), null );
	wp_enqueue_style( 'jdsofttech-header-footer', get_template_directory_uri() . '/assets/css/header-footer.css', array(), null );

	if(is_page_template( 'tmp-login.php' )){

		wp_enqueue_style( 'jdsofttech-fontawesome-css', get_template_directory_uri() . '/assets/css/fontawesome-all.min.css', array(), null );
		wp_enqueue_style( 'jdsofttech-material-design-css', get_template_directory_uri() . '/assets/fonts/iconic/css/material-design-iconic-font.min.css', array(), null );
		wp_enqueue_style( 'jdsofttech-util-css', get_template_directory_uri() . '/assets/css/util.css', array(), null );
		wp_enqueue_style( 'jdsofttech-login-css', get_template_directory_uri() . '/assets/css/login.css', array(), null );
	}

	wp_enqueue_script('jquery');
	wp_enqueue_script( 'jdsofttech-bootstrapmin-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), null, true );
	wp_enqueue_script( 'jdsofttech-common-js', get_template_directory_uri() . '/assets/js/common.js', array(), null, true );
	wp_enqueue_script( 'jdsofttech-dataTablesmin-js', get_template_directory_uri() . '/assets/js/jquery.dataTables.min.js', array(), null, true );
	// wp_enqueue_script( 'jdsofttech-dataTables-jqueyui', get_template_directory_uri() . '/assets/js/dataTables.jqueryui.min.js', array(), null, true );
	// wp_enqueue_script( 'jdsofttech-dataTables-bootstrap', get_template_directory_uri() . '/assets/js/dataTables.bootstrap.min.js', array(), null, true );

	// wp_enqueue_script( 'jdsofttech-jspdf-debug', get_template_directory_uri() . '/assets/js/jspdf.debug.js', array(), null, true );
	// wp_enqueue_script( 'jdsofttech-html2pdf', get_template_directory_uri() . '/assets/js/html2pdf.js', array(), null, true );

	// wp_enqueue_script( 'jdsofttech-jquery.min', get_template_directory_uri() . '/assets/js/jquery-1.12.3.min.js', array(), null, true );
	// wp_enqueue_script( 'jdsofttech-jspdf', get_template_directory_uri() . '/assets/js/jspdf.min.js', array(), null, true );

	// wp_enqueue_script( 'jdsofttech-dataTables-responsive', get_template_directory_uri() . '/assets/js/responsive.bootstrap.min.js', array(), null, true );

	// wp_enqueue_script( 'jdsofttech-developer', get_template_directory_uri() . '/assets/js/developer.js', array(), null, true );	
 	// wp_enqueue_script( 'jdsofttech-jquery-js', get_template_directory_uri() . '/assets/js/jquery-1.12.4.js', array(), null, true );	

}
add_action( 'wp_enqueue_scripts', 'custom_jdsofttech_scripts',1 );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
// require get_template_directory() . '/inc/customizer.php';
//require get_template_directory() . '/TCPDF-master/tcpdf.php';
// require get_template_directory() . '/TCPDF-master/tcpdf_autoconfig.php';
// require get_template_directory() . '/TCPDF-master/tcpdf_barcodes_1d.php';
// require get_template_directory() . '/TCPDF-master/tcpdf_barcodes_2d.php';
// require get_template_directory() . '/TCPDF-master/tcpdf_import.php';
// require get_template_directory() . '/TCPDF-master/tcpdf_parser.php';
// require get_template_directory() . '/TCPDF-master/config/tcpdf_config.php';

add_action('customize_register','custom_theme_customizer');
function custom_theme_customizer( $wp_customize){

    //create panel for Mayo Foundationpress
    $wp_customize->add_panel('footer_copyright_panel', array(
        'priority'          => 2002,
        'title'             => __('Footer Options', 'foundationpress'),
        'description'       => __('Options for Theme Footer', 'foundationpress')
    ));

	$wp_customize->add_section('footer_copyright_sec', array(
        'priority'          => 1007,
        'panel'             => 'footer_copyright_panel',
        'title'             => __('Footer Copyright', 'foundationpress'),
        'description'       => __('Edit Footer Copyright Option', 'foundationpress')
    ));
    $wp_customize->add_setting('footer_copyright_text');
    $wp_customize->add_control( 'footer_copyright_text', array(
            'type'              => 'text',
            'section'           => 'footer_copyright_sec', 
            'label'             => __( 'Footer Copyright Text' ),
           ));   
}
/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}



add_action('wp_head', 'myplugin_ajaxurl');

function myplugin_ajaxurl() {

   echo '<script type="text/javascript">
            var ajaxurl = "' . admin_url('admin-ajax.php') . '";
           	var site_url = "'. site_url() .'";
           	var new_customer_url = "'. site_url('/add-new-customer/') .'";
           	var new_invoice_url = "'. site_url('/add-new-invoice/') .'";
           	var edit_invoice_url = "'. site_url('/edit-invoice/') .'";
         </script>';
}

/* Convert number to Words - Start */

$ones = array(
	"",
	" One",
	" Two",
	" Three",
	" Four",
	" Five",
	" Six",
	" Seven",
	" Eight",
	" Nine",
	" Ten",
	" Eleven",
	" Twelve",
	" Thirteen",
	" Fourteen",
	" Fifteen",
	" Sixteen",
	" Seventeen",
	" Eighteen",
	" Nineteen"
);
 
$tens = array(
	"",
	"",
	" Twenty",
	" Thirty",
	" Forty",
	" Fifty",
	" Sixty",
	" Seventy",
	" Eighty",
	" Ninety"
);
 
$triplets = array(
	"",
	" Thousand",
	" Million",
	" Billion",
	" Trillion",
	" Quadrillion",
	" Quintillion",
	" Sextillion",
	" Septillion",
	" Octillion",
	" Nonillion"
);
 
 // recursive fn, converts three digits per pass
function convertTri($num, $tri) {
	global $ones, $tens, $triplets;

	$r = (int) ($num / 1000);
	$x = ($num / 100) % 10;
	$y = $num % 100;

	$str = "";

	if ($x > 0)
		$str = $ones[$x] . " Hundred";

	if ($y < 20)
		$str .= $ones[$y];
	else
		$str .= $tens[(int) ($y / 10)] . $ones[$y % 10];

	if ($str != "")
		$str .= $triplets[$tri];

	if ($r > 0)
		return convertTri($r, $tri+1).$str;
	else
		return $str;
}

function convertNum($num) {
	$num = (int) $num;    

	if ($num < 0)
	return "Negative".convertTri(-$num, 0);

	if ($num == 0)
	return "Zero";

	return convertTri($num, 0);
}
 
/* Convert number to Words - Start */


// Update Title name with Customer name 

add_action('acf/save_post', 'save_post_type_post', 20); // fires after ACF
function save_post_type_post($post_id) {
	$post_type = get_post_type($post_id);
	if ($post_type != 'invoice') {
		return;
	}
	$post_title = get_field('cust_invoice_number', $post_id);
	$post_name = sanitize_title($post_title);
	$post = array(
		'ID' => $post_id,
		'post_name' => $post_name,
		'post_title' => $post_title
	);
	wp_update_post($post);
}


add_action('acf/save_post', 'save_post_type_post1', 20); // fires after ACF
function save_post_type_post1($post_id) {
	$post_type = get_post_type($post_id);
	if ($post_type != 'customer') {
		return;
	}
	$post_title = get_field('customer_name', $post_id);
	$post_name = sanitize_title($post_title);
	$post = array(
		'ID' => $post_id,
		'post_name' => $post_name,
		'post_title' => $post_title
	);
	wp_update_post($post);
}


// Create Backend database for invoice and Customer post type

register_post_type( 'customer',
    array(
        'labels' => array(
                'name' => __( 'Customers' ),
                'singular_name' => __( 'Customer' )
        ),
	    'public' => true,
	    'has_archive' => true,
	    'show_in_menu'        => true,
	    'show_in_nav_menus'   => true,
	    'show_in_admin_bar'   => true,
	    'show_ui'             => true,
	    'menu_name'           => __( 'Customers', 'jdsofttech' ),
	    )
);

register_post_type( 'invoice',
    array(
        'labels' => array(
                'name' => __( 'Invoices' ),
                'singular_name' => __( 'Invoice' )
        ),
	    'public' => true,
	    'has_archive' => true,
	    'show_in_menu'        => true,
	    'show_in_nav_menus'   => true,
	    'show_in_admin_bar'   => true,
	    'show_ui'             => true,
	    'menu_name'           => __( 'Invoices', 'jdsofttech' ),
	    )
);



function my_acf_input_admin_footer() { ?>

	<script type="text/javascript">
		(function($) {
			
			// Websites Repeater Quantity and Rate Calculation

			acf.add_action('append', function( $el ){			
				
				$( ".invc_amount_cls input" ).attr("readonly", "readonly");	

				$('#invc_website_cls .acf-row').not(".acf-clone").each(function(){					

					var quant_id = $(this).find(".invc_qaunt_cls input").attr("id");

					$('#'+quant_id).change(function(){

						var quant_no = $(this).val();
						var rat_no = $(this).parents().siblings(".invc_rate_cls").find("input" ).val();
						var total = quant_no*rat_no;
						$(this).parents().siblings(".invc_amount_cls").find("input" ).val(total);
					});

					var rate_id = $(this).find(".invc_rate_cls input").attr("id");
					$('#'+rate_id).change(function(){
						
						var rat_no1 = $(this).val();
						var quant_no1 = $(this).parents().siblings(".invc_qaunt_cls").find("input" ).val();
						var total = quant_no1*rat_no1;
						$(this).parents().siblings(".invc_amount_cls").find("input" ).val(total);

					});
				});
			
			});		
		
			// Dates Validation

			var start_date_key = 'field_5ae6d5778d40b'; 
			var end_date_key = 'field_5ae6d5988d40c'; 
			  
			if (typeof(acf) != 'undefined') {

			    acf.add_action('date_picker_init', function($input, args, $field) {
			    
			    	var key = $input.closest('.acf-field').data('key');
			
				    if (key == start_date_key) {
				    	
				        $input.datepicker().on('input change select', function(e) {
				          
					        var date = $(this).datepicker('getDate');
					          
					        date.setDate(date.getDate()+1);
					         
					       $('[data-key="'+end_date_key+'"] input.hasDatepicker').datepicker("option", "minDate", date);
				        });
			      	}			    

			    });

			}

			$('#cust_state_cls select').on('change', function(){
				var val = $(this).val();
				//alert(val);
				$(document).trigger("acf/update_field_groups");

				console.log(val);

			});


		})(jQuery);	

	</script>
	
	<?php
		
}

add_action('acf/input/admin_footer', 'my_acf_input_admin_footer');

// Get Selected Existing Customer-Id
add_action('wp_ajax_existing_customer', 'existing_customer_ajaxdata');
add_action('wp_ajax_nopriv_existing_customer', 'existing_customer_ajaxdata');
function existing_customer_ajaxdata(  $cust) {

	$cust = $_REQUEST['customer'];
	echo wp_send_json($cust);
	    
}

function acf_load_color_field_choices1( $field ) {    
   
 	
    $field['choices'] = array();

	if(isset($_GET['customer_id'] )){
	$cust = $_GET['customer_id'];

	$choices[] = get_post_meta($cust, 'customer_address_0_customer_state', true);
	    foreach( $choices as $choice ) {

	    	$field['choices'][ $choice ]= $choice;
	    	
	    }		
	}
    return $field;
    
}

add_filter('acf/load_field/name=customer_state', 'acf_load_color_field_choices1');	

 

add_action( 'parse_request', 'wpse132196_redirect_after_trashing_get' );
function wpse132196_redirect_after_trashing_get() {
    if ( array_key_exists( 'deleted', $_GET ) && $_GET['deleted'] == '1' ) {
        wp_redirect( home_url('/invoices/') );
        exit;
    }
}

// Copy Invoice 

add_action('wp_ajax_invoice_copy', 'invoice_copy_ajaxdata');
add_action('wp_ajax_nopriv_invoice_copy', 'invoice_copy_ajaxdata');

function invoice_copy_ajaxdata(){
	global $wpdb;
	if (! ( isset( $_REQUEST['invoice']) || isset( $_REQUEST['invoice'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}

	$post_id = $_REQUEST['invoice'];
	$post = get_post( $post_id );
 	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	if (isset( $post ) && $post != null) {
 
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'publish',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		$new_post_id = wp_insert_post( $args );
 	
 	 		
 	
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				if( $meta_key == '_wp_old_slug' ) continue;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 		echo wp_send_json($new_post_id);
		wp_redirect( site_url('?duplicate_invoice_id='.$new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}

// Rmeove Archieve page form theme

function wpa_parse_query( $query ){
	if(is_archive() && !is_admin()) {
	    global $wp_query;
	    $wp_query->set_404();
	    status_header( 404 );
	    nocache_headers();
	    include("404.php");
	    die;
	}
}
add_action( 'parse_query', 'wpa_parse_query' );

// Get Countries from custom Table

function acf_load_color_field_choices( $field ) {
    
    $field['choices'] = array();
    global $wpdb;

	$myrows = $wpdb->get_results( "SELECT * FROM wp_countries" );

	    foreach( $myrows as $choice ) {		
	    	    	//echo $choice ;
	    	$field['choices'][ $choice->id ]= $choice->name;
	    	
	    }		

    return $field;
    
}

add_filter('acf/load_field/name=customer_country', 'acf_load_color_field_choices');	

// Get State Based on Country

add_action('wp_ajax_country_state', 'state_ajaxdata');
add_action('wp_ajax_nopriv_country_state', 'state_ajaxdata');

function state_ajaxdata(){

	if(!empty($_REQUEST["country"])) {

		global $wpdb;
		$myrows = $wpdb->get_results( "SELECT * FROM wp_states WHERE country_id = '" . $_REQUEST["country"] . "'" );

		?>
		<option value="">Select State</option>
		<?php
		foreach($myrows as $state) {
		
			if(isset($_GET['customer_id'])){
				$cust = $_GET['customer_id'];

				$state1= get_post_meta($cust, 'customer_address_0_customer_state', true); ?>

				<option value="<?php echo $state->name; ?>" <?php if( $state->name!=$state1){ ?> selected="selected" <?php } ?>><?php echo $state->name; ?></option>

			<?php
			} else { ?>

				<option value="<?php echo $state->name; ?>"><?php echo $state->name; ?></option>

			<?php

			}
		}
	}
	exit;
}

// Get Edit Invocie-Id
add_action('wp_ajax_editing_invoice', 'editing_invoice_ajaxdata');
add_action('wp_ajax_nopriv_editing_invoice', 'editing_invoice_ajaxdata');
function editing_invoice_ajaxdata( ) {

	$invoiceid = $_REQUEST['inv_id'];
	echo wp_send_json($invoiceid);
	    
}



// Get pdf invoice Id
add_action('wp_ajax_pdf_invoice', 'pdf_invoice_ajaxdata');
add_action('wp_ajax_nopriv_pdf_invoice', 'pdf_invoice_ajaxdata');
function pdf_invoice_ajaxdata( ) {

	$pdf_id = $_REQUEST['pdf_id'];
	echo wp_send_json($pdf_id);

}


// Get pdf invoice Id
add_action('wp_ajax_mail_invoice', 'mail_invoice_ajaxdata');
add_action('wp_ajax_nopriv_mail_invoice', 'mail_invoice_ajaxdata');
function mail_invoice_ajaxdata( ) {

	$mail_id = $_REQUEST['mail_id'];
	echo wp_send_json($mail_id);
    
}

function get_theme_root121( $stylesheet_or_template = false ) {
        global $wp_theme_directories;

        if ( $stylesheet_or_template && $theme_root = get_raw_theme_root( $stylesheet_or_template ) ) {
                // Always prepend WP_CONTENT_DIR unless the root currently registered as a theme directory.
                // This gives relative theme roots the benefit of the doubt when things go haywire.
                if ( ! in_array( $theme_root, (array) $wp_theme_directories ) )
                        $theme_root = WP_CONTENT_DIR . $theme_root;
        } else {
                $theme_root = WP_CONTENT_DIR . '/themes';
        }

        return apply_filters( 'theme_root', $theme_root );
}


// Redirect page only can use amdin user.

add_action( 'template_redirect', 'redirect_non_logged_users_to_specific_page' );
function redirect_non_logged_users_to_specific_page() {

	if ((!is_user_logged_in() && is_front_page()) || (!is_user_logged_in() && is_page('add-new-customer')) || (!is_user_logged_in() && is_page('add-new-invoice')) || (!is_user_logged_in() && is_page('customers')) || (!is_user_logged_in() && is_page('edit-invoice')) || (!is_user_logged_in() && is_page('add-new-invoice'))  ) {
	//if (! current_user_can('administrator') ){

		global $wp_query;
    	wp_redirect( home_url('/log-in/') ); 
    	//$wp_query->set_404();

	}
}



//for wp-login.php and wp-admin page redirect to log-in page

add_action( 'init','redirect_login_page' );
function redirect_login_page(){


    global $pagenow;

    $page = site_url('/login/');

	if( 'wp-login.php' == $pagenow ) {
	 	wp_redirect( home_url('/log-in/') );     
	  
	}

}

// After logout Redirect to Site URL

function go_home(){
  wp_redirect( home_url() );
  exit();
}
add_action('wp_logout','go_home');

/*
add_action( 'wp_ajax_siteWideMessage', 'wpse_sendmail' );
add_action( 'wp_ajax_nopriv_siteWideMessage', 'wpse_sendmail' );
function wpse_sendmail(){
  
  $invoice_id = $_REQUEST['invoiceid'];
  
  //echo wp_send_json($invoice_id);
	
	$emailid = 'submit_email_'.$invoice_id;

	if(isset($_POST[$emailid])){

	    //$invoice_id = $_POST['mail_invoice_id']; 

	    $customer = get_post_meta($invoice_id, 'cust_invoice_customer', true );
	    echo json_encode($customer);

	    $customer_email = get_field('customer_email', $customer->ID);

	    if( $_REQUEST['email_radio_value'] == 'radio_custom_email' ){
	       
	       $to = $_REQUEST['custom_emailid'];
	      

	    }elseif( $_REQUEST['email_radio_value'] == 'radio_customer_email'){

	       $to = get_field('customer_email', $customer->ID);

	    }

	    // $to = "nbhalala.jd@gmail.com";
	    $subject = "My subject";

	    $invoice_no = get_field('cust_invoice_number', $invoice_id);
	       
	    if( have_rows('customer_address', $customer->ID) ): 
	      while ( have_rows('customer_address', $customer->ID) ) : the_row(); 
	        $street = get_sub_field('customer_street', $customer->ID); 
	        $zipcode = get_sub_field('customer_zipcode', $customer->ID);
	        $state = get_sub_field('customer_state', $customer->ID);
	        $city = get_sub_field('customer_city', $customer->ID);

	        $otherinfo = '<div class="customer" style="background: #e7e7e8;border: 8px solid #bcbdc0;padding: 78px 0px 70px 40px; max-height: 500px; position: relative;"> 
	                        <span style="text-transform: uppercase;color: #818285;font-size: 20px;display: block;margin-bottom: 10px;"> Customer: </span> <a href="'. get_permalink($customer->ID) .'" style="color: #000; font-size: 30px;display: block; margin-bottom: 10px; text-decoration: none;outline: none;">'. get_field('customer_name', $customer->ID) .'</a>              
	                        <p style="font-size: 24px; color: #333; margin-bottom: 10px; line-height: 40px; margin: 0;">'. $street .'<br>'. $zipcode . ' ' . $state . '<br>' .
	                          'Ph : + '. get_field('customer_phone_no', $customer->ID).
	                        '</p>
	                      </div>';        
	      endwhile;
	    endif;


	    global $website_content, $tbody, $tbodyclose, $total;            
	    $website_content = '';   
	    if( have_rows('cust_invoice_website', $invoice_id) ):  
	    $tbody = '<tbody>';
	          while ( have_rows('cust_invoice_website', $invoice_id) ) : the_row(); 
	          
	            $website_content .= '<tr style="background-color: rgba(0,0,0,.05);">
	                <td class="col-description" style="border-right: 4px solid #fff; padding: 15px; line-height: 20px; text-align: left; vertical-align: top;color: #000; font-size: 18px;">'. get_sub_field('cust_invoice_desc', $invoice_id) .'</td>
	                <td class="col-description" style="border-right: 4px solid #fff; padding: 15px; line-height: 20px; text-align: left; vertical-align: top;color: #000; font-size: 18px;">'. get_sub_field('cust_invoice_quantity', $invoice_id) .'</td>
	                <td class="col-description" style="border-right: 4px solid #fff; padding: 15px; line-height: 20px; text-align: left; vertical-align: top;color: #000; font-size: 18px;">'. get_sub_field('cust_invoice', $invoice_id) .'</td>
	                <td class="col-amount" style="padding: 15px; line-height: 20px; text-align: right; vertical-align: top; color: #2186c8; font-size: 18px;"> $'. get_sub_field('cust_invoice_amount', $invoice_id) .'</td>
	              </tr>
	              <tr style="background: #faf9fc;height: 30px;">             
	                <td class="col-description" style="border-right: 4px solid #fff;"></td>                       
	                <td class="col-description" style="border-right: 4px solid #fff;"></td>
	                <td class="col-description" style="border-right: 4px solid #fff;"> </td>
	                <td class="col-amount"></td>
	              </tr>';
	        $total += intval( get_sub_field('cust_invoice_amount', $invoice_id));
	        endwhile; 
	        $tbodyclose = '</tbody>
	                      <tfoot>
	                        <tr>
	                          <td colspan="3" style="padding: 15px; line-height: 20px; text-align: left; vertical-align: top;"><div class="total" style="background-color: #2186c8; color: #fff; padding: 20px 0px; min-width: 150px; text-transform: uppercase; font-size: 20px; float: right; text-align: center;">Total</div></td>
	                          <td style="padding: 15px; line-height: 20px; text-align: left; vertical-align: top;"><div class="total amount" style="width: 94%; border-left: 2px solid #fff;text-align: right; background-color: #2186c8; color: #fff; padding: 20px 15px; min-width: 150px; text-transform: uppercase; font-size: 20px; float: right;"> $'. $total .'</div></td>
	                        </tr>
	                      </tfoot>';
	    endif;


	    $invoice_date = get_field('cust_invoice_invdate', $invoice_id); 
	    $due_date = get_field('cust_invoice_duedate', $invoice_id);                                               

	    $txt = '<!DOCTYPE html>
	          <html lang="en">
	            <head>
	              <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	              <meta name="viewport" content="width=device-width, initial-scale=1">
	              <style>
	               h1.logo_cls{
	                  color:#fff;
	               }
	               .customer_afimage{
	                width:94px;
	                height86px;
	                border-right: 1px solid #d1d2d4; 
	               }
	              </style>          
	            </head>
	            <body>
	            <table cellspacing="0">             
	              <tr>
	                <td>
	                  <section class="content-section single_invoice_div" style="background-color: #ffffff;padding: 0px 70px;min-height: 600px; margin: 0 auto;width:1250px;">              
	                    <div class="top-heading" style=" background: url(https://jdsofttech.com/invoice/wp-content/themes/jdsofttech/assets/img/ribbon.png) no-repeat center center;width: 619px; height: 75px;background-size: cover;text-align: center;margin:0 auto;">
	                      <h1 class="logo_cls" style="margin: 0;color:#fff; line-height: 70px;font-size: 36px;"> Invoice number: '. $invoice_no .' </h1>
	                    </div>
	                    <div class="logo-block-row" style=" position: relative;text-align: center; margin: 10px 0px; background: url(https://jdsofttech.com/invoice/wp-content/themes/jdsofttech/assets/img/logo-block-row-line.png) no-repeat center;background-size: contain;"> 
	                      <a class="logo" href="'.site_url() .'"><img src="https://jdsofttech.com/invoice/wp-content/themes/jdsofttech/assets/img/logo.png" alt="JDsofttech"/ style=" display: inline-block;max-width: 300px;position: relative;z-index: 2;"> </a> 
	                    </div>
	                    <div class="supplier-customer-box-row">
	                      <div class="table-responsive-md">
	                          <table width="0" border="0" cellpadding="0" cellspacing="0" style="width: 100%;border-top: 1px solid #d1d2d4;margin-bottom: 50px; margin-top: 45px;">
	                            <tbody>
	                              <tr>
	                                <td class="left-colum" style="width: 50%; padding: 0px 0px 0px 15px; vertical-align: top; border-right: 1px solid #d1d2d4;">

	                                  <table cellspacing="0" cellpadding="-8" width="101.5%">
	                                    <tr><td style="text-align: right; vertical-align: bottom;"><img src="https://jdsofttech.com/invoice/wp-content/themes/jdsofttech/assets/img/customer-ribon.png)" align="right" style="display:block;text-align: right; vertical-align: top; "></td></tr>
	                                    <tr>
	                                      <td>
	                                        <div class="supplier" style="padding: 0px 0px 15px 0px;"> <span style="text-transform: uppercase;color: #818285;font-size: 20px;display: block;margin-bottom: 10px;"> supplier </span> <a href="'. site_url() .'" style="color: #2185c7; font-size: 30px; display: block; text-transform: uppercase; margin-bottom: 10px; text-decoration: none; outline: none;"> JD Softtech INC . </a>
	                                          <p style="font-size: 24px;color: #333;margin-bottom: 10px;line-height: 40px;"> Web & Software Developments Company </p>
	                                        </div>
	                                        <div class="address" style="padding: 15px 0px 15px 0px; margin-bottom: 70px;"> <span style="text-transform: uppercase;color: #818285;font-size: 20px;display: block;margin-bottom: 10px;"> Address </span>
	                                          <p style="font-size: 24px;color: #333;margin-bottom: 10px;line-height: 40px;"> 903 Main Street, Hanson,<br>
	                                            02341,MA,<br>
	                                            USA </p>
	                                          <ul style="list-style: none; padding: 0;">
	                                            <li style="display: block;margin-left:0;"> <span style="text-transform: uppercase;color: #818285;font-size: 20px;display: inline-block;margin-bottom: 10px;"> Phone: </span> <a href="tel:+1 (781) 9744568" style="font-size: 24px;color: #2186c8; text-decoration: none; outline: none; display: inline-block;">
	                                              <p style="font-size: 24px;color: #333;margin-bottom: 10px;line-height: 40px;">+1 (781) 9744568 </p>
	                                              </a> </li>
	                                            <li style="display: block;margin-left:0;"> <span style="text-transform: uppercase;color: #818285;font-size: 20px;display: inline-block;margin-bottom: 10px;"> Website: </span> <a href="https://www.jdsofttech.com/" style=" font-size: 24px; color: #2186c8;"> www.jdsofttech.com </a> </li>
	                                            <li style="display: block;margin-left:0;"> <span style="text-transform: uppercase;color: #818285;font-size: 20px;display: inline-block;margin-bottom: 10px;"> E-mail: </span> <a href="mailto:info@jdsofttech.com" style=" font-size: 24px; color: #2186c8;"> info@jdsofttech.com </a> </li>
	                                          </ul>
	                                        </div>


	                                        </td>
	                                        </tr>
	                                  </table>


	                                </td>

	                                <td class="right-culum customerinfo" style="width: 50%; vertical-align: top; padding: 0px;">
	                                  '.$otherinfo . '                              
	                                </td>
	                              </tr>
	                            </tbody>
	                            <tfoot>
	                              <tr>                           
	                                <td class="left-colum foot-left-col" style="background: #2186c8; padding: 15px 30px; font-size: 30px; color: #fff; border-right: 1px solid #ffffff; border-bottom: 0;"> Date : '. $invoice_date .'</td>
	                                <td class="right-colum foot-right-col" style="background: #e7e7e8; padding: 15px 30px; font-size: 30px; color: #818285; text-align: right; border-bottom: 0;"> Date : '. $due_date .'</td>
	                              </tr>
	                            </tfoot>
	                          </table>
	                      </div>
	                    </div>
	                    <div class="description-amount-table-row">
	                      <div class="table-responsive-sm">
	                        <table class="table-striped" style="margin-bottom: 50px;width: 100%; border-collapse: collapse;">
	                          <thead style="background-color: #231f20;color: #fff; text-transform: uppercase; font-size: 20px; font-weight: normal;">
	                              <tr>
	                                <th class="col-description" style="border-right: 4px solid #fff; padding: 15px; line-height: 20px; text-align: left; vertical-align: top;"> Description </th>
	                                <th class="col-description" style="border-right: 4px solid #fff; padding: 15px; line-height: 20px; text-align: left; vertical-align: top;"> Quantity </th>
	                                <th class="col-description" style="border-right: 4px solid #fff; padding: 15px; line-height: 20px; text-align: left; vertical-align: top;"> Rate </th>
	                                <th class="col-amount" style="padding: 15px; line-height: 20px; text-align: right; vertical-align: top;"> Amount </th>
	                              </tr>
	                          </thead>'. $tbody . $website_content . $tbodyclose .
	                        '</table>
	                      </div>
	                    </div>
	                    <div class="contenteditable" style=" padding: 50px 0px;">
	                      <p style="font-size: 20px; color: #818285; margin: 0px;line-height: 30px;"> Amount in words: '. convertNum(  $total ) .' Dolloars Only. </p>
	                      <p style="font-size: 20px; color: #818285; margin: 0px;line-height: 30px;"> Declaration: We declare that this invoice shows the actual price for LeadersUp and all particulars are true and  correct. </p>
	                    </div>
	                    <div class="thank-you-text" style="padding: 25px 0px; text-align: center; font-size: 30px; color: #818285; border-top: 1px solid #d1d2d4;"> Thank you for your business! </div>
	                  </section>
	                </td>
	                </tr>
	            </table>
	          </body></html>';

	    $headers = "MIME-Version: 1.0" . "\r\n";
	    $headers .= "Content-type:text/html; charset=iso-8859-1" . "\r\n";
	    // $headers .= "Content-type: application/json" . "\r\n";
	    // $headers .= "From: nbhalala.jd@gmail.com";
	    $headers .= "From: vpatel@taskme.biz";

	    wp_mail($to,$subject,$txt,$headers);

    die();
	}
}
*/