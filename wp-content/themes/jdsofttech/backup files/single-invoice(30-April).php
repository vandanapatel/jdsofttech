<?php 
get_header();

global $post;

?>
<section class="content-section single_invoice_div">
  	<div class="top-heading">
    	<h1> Invoice number: <?php the_field('invoice_number'); ?> </h1>
  	</div>

  	<div class="logo-block-row"> 
  		<a class="logo" href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="JDsofttech"/> </a> 
 	</div>

 	<div class="row">
	    <div class="col-md-12 single_delete_invoice text-right">
	 		<?php $link =  wp_nonce_url( get_bloginfo('url') . "/wp-admin/post.php?action=delete&amp;post=" . $post->ID, 'delete-post_' . $post->ID); ?>

	 		<a href="<?php echo $link; ?>" class="btn btn-primary a-btn-slide-text">
		       <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
		        <span><strong>Delete</strong></span>            
		    </a>
	 	</div>
 	</div>

  	<div class="supplier-customer-box-row">
	    <div class="table-responsive-md">
	      	<table width="0" border="0" cellpadding="0" cellspacing="0">
		        <tbody>
		          <tr>
		            <td class="left-colum">
		            	<div class="supplier"> <span> supplier </span> <a href="#"> JD Softtech INC . </a>
		                	<p> Web & Software Developments Company </p>
		              	</div>
		              	<div class="address"> <span> Address </span>
			                <p> 903 Main Street, Hanson,</br>
			                  02341,MA,</br>
			                  USA </p>
			                <ul>
			                  <li> <span> Phone: </span> <a href="#">
			                    <p>+1 (781) 9744568 </p>
			                    </a> </li>
			                  <li> <span> Website: </span> <a href="#"> www.jdsofttech.com </a> </li>
			                  <li> <span> E-mail: </span> <a href="#"> info@jdsofttech.com </a> </li>
			                </ul>
			             </div>
		          	</td>
		            <td class="right-culum">
		            	<div class="customer"> <span> Customer: </span> <a href="#"> <?php the_field('invoice_customer_name'); ?> </a>
		                
			                <?php 
		               		if( have_rows('invoice_address') ): 
	      						while ( have_rows('invoice_address') ) : the_row();

			                	$street = get_sub_field('invoice_street'); 
			                	$zipcode = get_sub_field('invoice_zipcode');
			                	$state = get_sub_field('invoice_state');
			                	$city = get_sub_field('invoice_city');

			                	echo '<p>' . $street. ', </br>';
			                	echo $zipcode . ' ' . $state . '</br>';
		                 		

			                	endwhile;
			                endif;
				            ?>
			               	Ph : +<?php the_field('invoice_phone_number'); ?></p>
		              	</div>
		          	</td>
		          </tr>
		        </tbody>
		        <tfoot>
		          <tr>
		          	<?php 
		          		$invoice_date = get_field('invoice_invoice_date'); 
		          		$due_date = get_field('invoice_due_date'); 

		          	?>
		            <td class="left-colum foot-left-col"> Date : <?php echo $invoice_date; ?> </td>
		            <td class="right-colum foot-right-col"> Date : <?php echo $due_date; ?> </td>
		          </tr>
		        </tfoot>
	      	</table>
	    </div>
 	</div>
  	<div class="description-amount-table-row">
	    <div class="table-responsive-sm">
	      <table class="table-striped">
	      	<thead>
				<?php 		       
	           	if( have_rows('invoice_websites') ):  ?>

		          	<tr>
			            <th class="col-description"> Description </th>
			            <th class="col-description"> Quantity </th>
			            <th class="col-description"> Rate </th>
			            <th class="col-amount"> Amount </th>
		          	</tr>
			        <tbody>
						<?php while ( have_rows('invoice_websites') ) : the_row(); ?>
					        <tr>
					        	
					            <td class="col-description"> <?php the_sub_field('description'); ?> </td>
								<td class="col-description"><?php the_sub_field('quantity'); ?> </td>
					            <td class="col-description"> <?php the_sub_field('rate'); ?> </td>
					            <td class="col-amount"> $<?php the_sub_field('amount'); ?> </td>
					        </tr>

					        <tr>
					            <td class="col-description"></td>						            
					            <td class="col-description"></td>
					            <td class="col-description"> </td>
					            <td class="col-amount"></td>
					        </tr>
						<?php 
						global $total;
						 $total += intval( get_sub_field('amount'));

		  			   	endwhile; ?>
		  			</tbody>

			        <tfoot>
			          <tr>
			            <td  colspan="3"><div class="total">Total</div></td>
			            <td><div class="total amount"> $<?php echo $total; ?></div></td>
			          </tr>
			        </tfoot>
              	<?php  
              	endif; ?>
					    
	        </thead>
	        
	      </table>
	    </div>
  	</div>
  	<div class="contenteditable">
  		
	    <p> Amount in words: <?php echo convertNum(  $total ); ?> Dolloars Only. </p>
	    <p> Declaration: We declare that this invoice shows the actual price for LeadersUp and all particulars are true and  correct. </p>
  	</div>
  	<div class="thank-you-text"> Thank you for your business! </div>
</section>


<?php get_footer(); ?>