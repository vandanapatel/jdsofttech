<?php 
// Template Name:Invoices

get_header();

global $post;

?>
  <div class="row">
      <div class="col-md-12 single_delete_invoice text-right">
        <a href="<?php echo site_url('/add-new-invoice/'); ?>" class="btn btn-primary a-btn-slide-text">
          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
          <span><strong>Add New Invoice</strong></span>            
        </a>
    </div>
  </div>

  <div class="invoices_main">
    <?php
 
    $args = array(
      'post_type'   => 'invoice',
      'orderby'   => 'ID',
      'order'     => 'DESC',
      'posts_per_page' => -1
     );
     
    $wp_Query = new WP_Query( $args );
    if( $wp_Query->have_posts() ) :
    ?>
   
      <table id="table-invoicelist" class="display" style="width:100%">
        <thead>
          <tr>
            <th> Invoice No. </th>
            <th> Customer Name </th>
            <th> Due Date </th>
            <th> Payment Status </th>
          </tr>
        </thead>
        <tbody>

        <?php  
          while( $wp_Query->have_posts() ) :
            $wp_Query->the_post(); ?>

              <tr>                
                  <td> <a href="<?php the_permalink(); ?>"><?php the_field('invoice_number'); ?></a> </td>
                  <td> <?php the_field('invoice_customer_name'); ?> </td>
                  <td> <?php the_field('invoice_due_date'); ?> </td>
                  <td> <?php echo 'Processing'; ?> </td>
              </tr>
          
          <?php 
          endwhile;
          wp_reset_postdata();
          ?>

        </tbody>            
        
      </table>

    <?php
    else :
      esc_html_e( 'No Invoices!', 'jdsofttech' );
    endif;
    ?>
  </div>


<?php get_footer(); ?>