<?php
/**
 * Jdsofttech functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Jdsofttech
 */

if ( ! function_exists( 'jdsofttech_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function jdsofttech_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Jdsofttech, use a find and replace
		 * to change 'jdsofttech' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'jdsofttech', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'jdsofttech' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'jdsofttech_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'jdsofttech_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function jdsofttech_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'jdsofttech_content_width', 640 );
}
add_action( 'after_setup_theme', 'jdsofttech_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function jdsofttech_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'jdsofttech' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'jdsofttech' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'jdsofttech_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function jdsofttech_scripts() {
	wp_enqueue_style( 'jdsofttech-style', get_stylesheet_uri() );

	wp_enqueue_script( 'jdsofttech-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'jdsofttech-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'jdsofttech_scripts' );

function custom_jdsofttech_scripts() {
	
	// Jdsofttech CSS

	
	wp_enqueue_style( 'jdsofttech-bootstrapmin-css', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), null );
	wp_enqueue_style( 'jdsofttech-style-css', get_template_directory_uri() . '/assets/css/style.css', array(), null );
	wp_enqueue_style( 'jdsofttech-responsive-css', get_template_directory_uri() . '/assets/css/responsive.css', array(), null );
	wp_enqueue_style( 'jdsofttech-dataTables-css', get_template_directory_uri() . '/assets/css/jquery.dataTables.min.css', array(), null );
	wp_enqueue_style( 'jdsofttech-dataTables-bootstrap-css', get_template_directory_uri() . '/assets/css/dataTables.bootstrap.min.css', array(), null );
	
	// Jdsofttech JS

	wp_enqueue_script('jquery');
	wp_enqueue_script( 'jdsofttech-bootstrapmin-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), null, true );
	wp_enqueue_script( 'jdsofttech-common-js', get_template_directory_uri() . '/assets/js/common.js', array(), null, true );
	wp_enqueue_script( 'jdsofttech-dataTablesmin-js', get_template_directory_uri() . '/assets/js/jquery.dataTables.min.js', array(), null, true );
	wp_enqueue_script( 'jdsofttech-dataTables-jqueyui', get_template_directory_uri() . '/assets/js/dataTables.jqueryui.min.js', array(), null, true );
	wp_enqueue_script( 'jdsofttech-dataTables-bootstrap', get_template_directory_uri() . '/assets/js/dataTables.bootstrap.min.js', array(), null, true );

	// wp_enqueue_script( 'jdsofttech-developer', get_template_directory_uri() . '/assets/js/developer.js', array(), null, true );	
 	// wp_enqueue_script( 'jdsofttech-jquery-js', get_template_directory_uri() . '/assets/js/jquery-1.12.4.js', array(), null, true );	

}
add_action( 'wp_enqueue_scripts', 'custom_jdsofttech_scripts',1 );

add_action('wp_head', 'myplugin_ajaxurl');

function myplugin_ajaxurl() {

   echo '<script type="text/javascript">
            var ajaxurl = "' . admin_url('admin-ajax.php') . '";
           	var site_url = "'. site_url() .'";
         </script>';
}
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}




/* Convert number to Words - Start */

$ones = array(
	"",
	" One",
	" Two",
	" Three",
	" Four",
	" Five",
	" Six",
	" Seven",
	" Eight",
	" Nine",
	" Ten",
	" Eleven",
	" Twelve",
	" Thirteen",
	" Fourteen",
	" Fifteen",
	" Sixteen",
	" Seventeen",
	" Eighteen",
	" Nineteen"
);
 
$tens = array(
	"",
	"",
	" Twenty",
	" Thirty",
	" Forty",
	" Fifty",
	" Sixty",
	" Seventy",
	" Eighty",
	" Ninety"
);
 
$triplets = array(
	"",
	" Thousand",
	" Million",
	" Billion",
	" Trillion",
	" Quadrillion",
	" Quintillion",
	" Sextillion",
	" Septillion",
	" Octillion",
	" Nonillion"
);
 
 // recursive fn, converts three digits per pass
function convertTri($num, $tri) {
	global $ones, $tens, $triplets;

	$r = (int) ($num / 1000);
	$x = ($num / 100) % 10;
	$y = $num % 100;

	$str = "";

	if ($x > 0)
		$str = $ones[$x] . " Hundred";

	if ($y < 20)
		$str .= $ones[$y];
	else
		$str .= $tens[(int) ($y / 10)] . $ones[$y % 10];

	if ($str != "")
		$str .= $triplets[$tri];

	if ($r > 0)
		return convertTri($r, $tri+1).$str;
	else
		return $str;
}

function convertNum($num) {
	$num = (int) $num;    

	if ($num < 0)
	return "Negative".convertTri(-$num, 0);

	if ($num == 0)
	return "Zero";

	return convertTri($num, 0);
}
 
/* Convert number to Words - Start */


// Update Title name with Customer name 

add_action('acf/save_post', 'save_post_type_post', 20); // fires after ACF
function save_post_type_post($post_id) {
	$post_type = get_post_type($post_id);
	if ($post_type != 'invoice') {
		return;
	}
	$post_title = get_field('invoice_customer_name', $post_id);
	$post_name = sanitize_title($post_title);
	$post = array(
		'ID' => $post_id,
		'post_name' => $post_name,
		'post_title' => $post_title
	);
	wp_update_post($post);
}


// Create Backend database for invoice post type

register_post_type( 'invoice',
    array(
        'labels' => array(
                'name' => __( 'Invoices' ),
                'singular_name' => __( 'Invoice' )
        ),
	    'public' => true,
	    'has_archive' => true,
	    'show_in_menu'        => true,
	    'show_in_nav_menus'   => true,
	    'show_in_admin_bar'   => true,
	    'show_ui'             => true,
	    'menu_name'           => __( 'Invoices', 'jdsofttech' ),
	    )
);


function my_acf_input_admin_footer() { ?>

	<script type="text/javascript">
		(function($) {
			
			// Websites Repeater Quantity and Rate Calculation

			acf.add_action('append', function( $el ){			
				
				$( ".csm_amount input" ).attr("readonly", "readonly");	

				$('#csm_website	.acf-row').not(".acf-clone").each(function(){					

					var quant_id = $(this).find(".csm_quantity input").attr("id");

					$('#'+quant_id).change(function(){

						var quant_no = $(this).val();
						var rat_no = $(this).parents().siblings(".csm_rate").find("input" ).val();
						var total = quant_no*rat_no;
						$(this).parents().siblings(".csm_amount").find("input" ).val(total);
					});

					var rate_id = $(this).find(".csm_rate input").attr("id");
					$('#'+rate_id).change(function(){
						
						var rat_no1 = $(this).val();
						var quant_no1 = $(this).parents().siblings(".csm_quantity").find("input" ).val();
						var total = quant_no1*rat_no1;
						$(this).parents().siblings(".csm_amount").find("input" ).val(total);

					});
				});
			
			});		
		
			// Dates Validation

			var start_date_key = 'field_5acb0bdfbc9ec'; 
			var end_date_key = 'field_5acb0c00bc9ed'; 
			  
			if (typeof(acf) != 'undefined') {

			    acf.add_action('date_picker_init', function($input, args, $field) {
			    
			    	var key = $input.closest('.acf-field').data('key');
			
				    if (key == start_date_key) {
				    	
				        $input.datepicker().on('input change select', function(e) {
				          
					        var date = $(this).datepicker('getDate');
					          
					        date.setDate(date.getDate()+1);
					         
					       $('[data-key="'+end_date_key+'"] input.hasDatepicker').datepicker("option", "minDate", date);
				        });
			      	}			    

			    });

			}

		})(jQuery);	

	</script>
	
	<?php
		
}

add_action('acf/input/admin_footer', 'my_acf_input_admin_footer');

// Get Selected Existing Customer-Id

add_action('wp_ajax_existing_customer', 'existing_customer_ajaxdata');
add_action('wp_ajax_nopriv_existing_customer', 'existing_customer_ajaxdata');
function existing_customer_ajaxdata(  $cust) {

	$cust = $_REQUEST['customer'];
	echo wp_send_json($cust);

}
 

add_action( 'parse_request', 'wpse132196_redirect_after_trashing_get' );
function wpse132196_redirect_after_trashing_get() {
    if ( array_key_exists( 'deleted', $_GET ) && $_GET['deleted'] == '1' ) {
        wp_redirect( home_url('/invoices/') );
        exit;
    }
}

// Copy Invoice 

/*add_action('wp_ajax_invoice_copy', 'invoice_copy_ajaxdata');
add_action('wp_ajax_nopriv_invoice_copy', 'invoice_copy_ajaxdata');
function invoice_copy_ajaxdata(  $cust) {

	$cust = $_REQUEST['customer'];
	echo wp_send_json($cust);

}*/

add_action('wp_ajax_invoice_copy', 'invoice_copy_ajaxdata');
add_action('wp_ajax_nopriv_invoice_copy', 'invoice_copy_ajaxdata');

function invoice_copy_ajaxdata(){
	global $wpdb;
	if (! ( isset( $_REQUEST['customer']) || isset( $_REQUEST['customer'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}

	$post_id = $_REQUEST['customer'];
	$post = get_post( $post_id );
 	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	if (isset( $post ) && $post != null) {
 
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'publish',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		$new_post_id = wp_insert_post( $args );
 	
 	 		
 	
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				if( $meta_key == '_wp_old_slug' ) continue;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 		echo wp_send_json($new_post_id);
		wp_redirect( site_url('?duplicate_invoice_id='.$new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}

// Rmeove Archieve page form theme
function wpa_parse_query( $query ){
	if(is_archive() && !is_admin()) {
	    global $wp_query;
	    $wp_query->set_404();
	    status_header( 404 );
	    nocache_headers();
	    include("404.php");
	    die;
	}
}
add_action( 'parse_query', 'wpa_parse_query' );

// Get state

/*$country=$_GET["acf[field_5acaf8e18a012][0][field_5ae40be385af4]"];
$request_url = "http://ws.geonames.org/countryInfo?country=" . $country;
$ch = curl_init($request_url);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$xml_raw = curl_exec($ch);
$countryxml = simplexml_load_string($xml_raw);
echo '<select name="statename">' . "\n";
echo '<option selected="selected" value="">Select a state</option>' . "\n";
foreach ($countryxml->country as $link)  {
    $geonameid = $link->geonameId;
    $stateurl = "http://ws.geonames.org/children?geonameId=" . $geonameid;
    $statexml = simplexml_load_file($stateurl);
        foreach ($statexml->geoname as $link)  {
            $statename = $link->name;
            echo '<option value="' . $statename . '">' . $statename . '</option>' . "\n";
        }
    }
echo '</select>';*/