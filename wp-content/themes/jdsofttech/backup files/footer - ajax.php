<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Jdsofttech
 */

?>
<script type="text/javascript">
	jQuery(window).on('load', function() {

	    // jQuery(".acf-form-submit input").prop('type', 'button').prop("id","buttonForm").prop("name","buttonForm");
	    // jQuery(".acf-form-submit input").prop('type', 'button').addClass("buttonForm");
	   jQuery(".acf-form-submit input").hide();
	});
	jQuery(document).ready(function(){

		function getUrlVars(){
		    var vars = [], hash;
		    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		    for(var i = 0; i < hashes.length; i++)
		    {
		        hash = hashes[i].split('=');
		        vars.push(hash[0]);
		        vars[hash[0]] = hash[1];
		    }
		    return vars;
		}
		var me = getUrlVars()["invoice_id"];

		//alert(mre);
		// Calculation with Quantity and Rate with Repeader Values 

		jQuery( ".csm_quantity:first input" ).on('change',function() {

			var quant = jQuery( this ).val();				
			var rat = jQuery( ".csm_rate input" ).val();			
			var total_amount = quant*rat;
			jQuery(".csm_amount:first input" ).val(total_amount);

		});

		jQuery( ".csm_rate:first input" ).on('change',function() {

			var quant = jQuery(".csm_quantity input" ).val();				
			var rat = jQuery( this ).val();
			var total_amount = quant*rat;
			jQuery( ".csm_amount:first input" ).val(total_amount);

		});

		if (jQuery.isNumeric(me)){
			jQuery("#csm_customer input[value='existing']").prop('checked');
	    	jQuery("#csm_customer input[value='new']").removeAttr('checked','checked');	
		}else{
			//alert("Hii");
		} 

		// Select Customer AJAX

		jQuery('#acf-field_5ad4430001556').on('change', function() {

		});
   	    var custome = jQuery("#csm_customer input").val();
   	    alert(custome);
		jQuery("#csm_customer input").on('change', function() {
		    var customer_val = jQuery(this).val();
		   	if(customer_val == 'existing'){
  				jQuery(".acf-form-submit input").prop('type', 'button').addClass("buttonForm").prop("id","buttonForm").prop("name","buttonForm");
  				jQuery(".acf-form-submit input").prop('type', 'button').removeClass("submitForm");
				jQuery(".acf-form-submit input").hide();
		   	}else if(customer_val == 'new'){
				jQuery(".acf-form-submit input").prop('type', 'submit').addClass("submitForm");
				jQuery(".acf-form-submit input").prop('type', 'submit').removeClass("buttonForm");
				jQuery(".acf-form-submit input").show();
		   	}
		});



		var ajaxurl= '<?php echo admin_url('admin-ajax.php') ; ?>';

	    jQuery("#acf-field_5ad4430001556").on('change',function(){
					
			var exe_cust = jQuery( '#acf-field_5ad4430001556 option:selected' ).val();

            jQuery.ajax({
	              	url: ajaxurl,
	              	async: true,
	              	data: {action  : 'existing_customer',customer: exe_cust},
	              	success: function(data){
	              		alert(data);
	              		/*if(data){
	              			//alert(data);						  

						   	jQuery(".acf-field").removeClass('hidden-by-conditional-logic');
						    jQuery(".acf-field input").removeAttr('disabled');
							jQuery(".acf-field select").removeAttr('disabled');

						    var result1 =JSON.parse(data);;

						   	var result = result1['invoice_data'];
							//alert(result);
						   	
						   	var id = result['id'];							
							var invoice_no = result['inv_no'];
							var cust_name = result['inv_custname'];
								
							var street = result['inv_street'];
							var zipcode = result['inv_zipcode'];
							var state = result['inv_state'];
							var city = result['inv_city'];

							var phone_no = result['inv_phoneno'];
							var invoice_date = result['inv_invdate'];
							var due_date = result['inv_duedate'];

							var website = result['inv_website'];							
							
							var i;
							var output = ""; 

							/*var webdesc = result['inv_webdesc'];
							var quality = result['inv_quality'];
							var rate = result['inv_rate'];
							var amount = result['inv_amount'];
					

							jQuery.each(webdesc, function(key, value) {
								console.log(value+' / '+key);
								//jQuery('#csm_description input').val(value);
								var input = '<tr class="acf-row" data-id="5add5dcbe4980" style=""><td class="acf-row-handle order ui-sortable-handle" title="Drag to reorder"><a class="acf-icon -collapse small" href="#" data-event="collapse-row" title="Click to toggle"></a><span>'+key+'</span></td><td id="csm_description" class="acf-field acf-field-text acf-field-5acb0ca5fec22 csm_description'+key+'" data-name="description" data-type="text" data-key="field_5acb0ca5fec22"><div class="acf-input"><div class="acf-input-wrap"><input type="text" id="acf-field_5acb0c57fec21-5add5dcbe4980-field_5acb0ca5fec22" name="acf[field_5acb0c57fec21][5add5dcbe4980][field_5acb0ca5fec22]" value="'+value+'"></div></div></td>';

								var input = input+'<td class="acf-row-handle remove"><a class="acf-icon -plus small acf-js-tooltip" href="#" data-event="add-row" title="Add row"></a><a class="acf-icon -minus small acf-js-tooltip" href="#" data-event="remove-row" title="Remove row"></a></td></tr>';
									
								jQuery('#csm_website tbody').append(input);

							});							
							jQuery.each(quality, function (key, value) {							        
						        console.log(value+' / '+key);
						        var input = '<td id="csm_quantity" class="acf-field acf-field-number acf-field-5acb0caefec23 csm_quantity'+key+' -collapsed-target" data-name="quantity" data-type="number" data-key="field_5acb0caefec23"><div class="acf-input"><div class="acf-input-wrap"><input type="number" id="acf-field_5acb0c57fec21-5add5dcbe4980-field_5acb0caefec23" name="acf[field_5acb0c57fec21][5add5dcbe4980][field_5acb0caefec23]" min="1" step="any" value="'+value+'"></div></div></td>';

						        jQuery(input).insertAfter('.csm_description'+key);

							});	
							jQuery.each(rate, function (key, value) {							        
						         console.log(value+' / '+key);
						        var input = '<td id="csm_rate" class="acf-field acf-field-number acf-field-5acb0cb5fec24 csm_rate'+key+'" data-name="rate" data-type="number" data-key="field_5acb0cb5fec24" data-required="1"><div class="acf-input"><div class="acf-input-wrap"><input type="number" id="acf-field_5acb0c57fec21-5add5dcbe4980-field_5acb0cb5fec24" name="acf[field_5acb0c57fec21][5add5dcbe4980][field_5acb0cb5fec24]" step="any" required="required" value="'+value+'"></div></div></td>';

						        jQuery(input).insertAfter('.csm_quantity'+key);

							});	
							jQuery.each(amount, function (key, value) {							        
						        console.log(value+' / '+key);
						        var input = '<td id="csm_amount" class="acf-field acf-field-text acf-field-5acb4f42ac7e5 csm_amount'+key+'" data-name="amount" data-type="text" data-key="field_5acb4f42ac7e5"><div class="acf-input"><div class="acf-input-wrap"><input type="text" id="acf-field_5acb0c57fec21-5add5dcbe4980-field_5acb4f42ac7e5" name="acf[field_5acb0c57fec21][5add5dcbe4980][field_5acb4f42ac7e5]" value="'+value+'"></div></div></td>';

						        jQuery(input).insertAfter('.csm_rate'+key);

							});	*/

							//$.each(result, function(i) {
							/* for (i = 1; i <= website; i++) { 
								var webdesc = result['inv_webdesc'];
								var quality = result['inv_quality'];
								var rate = result['inv_rate'];
								var amount = result['inv_amount'];
								//alert(webdesc);
								// jQuery.each(webdesc, function(i) {
								// 	//alert(webdesc);
								// });
							   var products = [{name: "first product"}, {name: "second product"}, {name: "third product"}];

							    for(var i = 0; i < products.length; i++) {
							        var product = products[i];
							       // $.get("https://api.github.com").then(function(){
							            console.log(product.name);
							        //});
							    }
								
								var input = '<tr class="acf-row" data-id="5add5dcbe4980" style=""><td class="acf-row-handle order ui-sortable-handle" title="Drag to reorder"><a class="acf-icon -collapse small" href="#" data-event="collapse-row" title="Click to toggle"></a><span>'+i+'</span></td><td id="csm_description" class="acf-field acf-field-text acf-field-5acb0ca5fec22 csm_description" data-name="description" data-type="text" data-key="field_5acb0ca5fec22"><div class="acf-input"><div class="acf-input-wrap"><input type="text" id="acf-field_5acb0c57fec21-5add5dcbe4980-field_5acb0ca5fec22" name="acf[field_5acb0c57fec21][5add5dcbe4980][field_5acb0ca5fec22]" value="'+webdesc+'"></div></div></td><td id="csm_quantity" class="acf-field acf-field-number acf-field-5acb0caefec23 csm_quantity -collapsed-target" data-name="quantity" data-type="number" data-key="field_5acb0caefec23"><div class="acf-input"><div class="acf-input-wrap"><input type="number" id="acf-field_5acb0c57fec21-5add5dcbe4980-field_5acb0caefec23" name="acf[field_5acb0c57fec21][5add5dcbe4980][field_5acb0caefec23]" min="1" step="any" value="'+quality+'"></div></div></td><td id="csm_rate" class="acf-field acf-field-number acf-field-5acb0cb5fec24 csm_rate" data-name="rate" data-type="number" data-key="field_5acb0cb5fec24" data-required="1"><div class="acf-input"><div class="acf-input-wrap"><input type="number" id="acf-field_5acb0c57fec21-5add5dcbe4980-field_5acb0cb5fec24" name="acf[field_5acb0c57fec21][5add5dcbe4980][field_5acb0cb5fec24]" step="any" required="required" value="'+rate+'"></div></div></td><td id="csm_amount" class="acf-field acf-field-text acf-field-5acb4f42ac7e5 csm_amount" data-name="amount" data-type="text" data-key="field_5acb4f42ac7e5"><div class="acf-input"><div class="acf-input-wrap"><input type="text" id="acf-field_5acb0c57fec21-5add5dcbe4980-field_5acb4f42ac7e5" name="acf[field_5acb0c57fec21][5add5dcbe4980][field_5acb4f42ac7e5]" value="'+amount+'"></div></div></td><td class="acf-row-handle remove"><a class="acf-icon -plus small acf-js-tooltip" href="#" data-event="add-row" title="Add row"></a><a class="acf-icon -minus small acf-js-tooltip" href="#" data-event="remove-row" title="Remove row"></a></td></tr>';
									
								output+=input;
								//alert(input);
								
							}
							
						
							//});
						
							var rows = jQuery('#csm_website tbody tr').length;
							//alert(rows);
							if(rows == 1){
								jQuery('#csm_website tbody').append(output);
								
							}else if(rows >= 1){
								jQuery('#csm_website tbody').empty();
								jQuery('#csm_website tbody').append(output);
							}

							jQuery(".csm_invoice_number input").val(invoice_no);
						    jQuery(".csm_customer_name input").val(cust_name);

						    jQuery(".csm_street input").val(street);
						    jQuery(".csm_zipcode input").val(zipcode);
						    jQuery(".csm_state select").val(state);
						    jQuery(".csm_city input").val(city);

						    jQuery(".csm_phone_number input").val(phone_no);
						    jQuery(".csm_invoice_date input").val(invoice_date);
						    jQuery(".csm_due_date input").val(due_date);

							var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?invoice_id='+id;
						    window.history.pushState({path:newurl},'',newurl); */

							var site_url = "<?php echo site_url(); ?>";
						    window.location.href = site_url+'?invoice_id='+data;
						   
						    //jQuery(".acf-form-submit input").show();

						//}

	              	}
	        });
           
		}); 
	  

	});

</script>


<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/datepicker.js"></script>


<?php wp_footer(); ?>

</body>
</html>
