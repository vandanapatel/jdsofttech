<?php 
// Template Name:Invoices

get_header();

global $post;

// echo '<pre>';
//   print_r($post);
// echo '<pre>';

//$get_invoice = $_GET['invoice_no'];

// if($get_invoice != ''){
//   echo 'Hii'.'<br>';

//   echo $get_invoice.'<br>';
//  // echo get_field('invoice_number');
// }
?>

  <div class="invoices_main">
    <?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
      'post_type'   => 'invoice',
      'orderby'   => 'ID' ,
      'order'     => 'DESC' ,
      'posts_per_page' => 9,
      'paged' => $paged
     );
     
    $wp_Query = new WP_Query( $args );
    if( $wp_Query->have_posts() ) :
    ?>
    <?php /* ?>
      <ul class="invoices_ul">
        <?php
          while( $wp_Query->have_posts() ) :
            $wp_Query->the_post();
            ?>
              <li class="invoices_li "><a href="<?php echo the_permalink(); ?>" class="invoices_a"><span> <?php echo get_the_title(); ?></span></a></li>
            <?php
          endwhile;
          wp_reset_postdata();
        ?>
      </ul>

      <div class="row">
        <div class="col-xs-12  text-center">
          <nav aria-label="Page navigation example ">
              
            <?php

              $big = 999999999; // need an unlikely integer
              $translated = __( '', 'jdsofttech' ); // Supply translatable string
              $current = max( 1, get_query_var('paged') );
              $return  = paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => $current,
                'total' => $wp_Query->max_num_pages,
                'before_page_number' => '<span class="screen-reader-text">'.$translated.' </span>',
                'prev_text'          => __('«'),
                'next_text'          => __('»'),
                'type' => 'list'
              ) );
          
              $return = str_replace( "<ul class='page-numbers'>", '<ul class="pagination custom_stacked_pagination">', $return );                        
              $return = str_replace( "<li>", '<li class="page-item">', $return );                      
              echo $return; ?>
          
          </nav>
        </div>
      </div>
      <?php */ ?>
      <table id="table-invoicelist" class="display" style="width:100%">
        <thead>
          <tr>
            <th class="">Invoice No.</th>
            <th class="">Customer Name</th>
            <th class="">Due Date</th>
            <th class="">Payment Status</th>
          </tr>
        </thead>
        <tbody>

        <?php  
          while( $wp_Query->have_posts() ) :
            $wp_Query->the_post(); ?>

              <tr>                
                  <td class=""> <?php the_field('invoice_number'); ?> </td>
                  <td class=""><?php the_field('invoice_customer_name'); ?> </td>
                  <td class=""> <?php the_field('invoice_due_date'); ?> </td>
                  <td class=""> <?php echo 'Processing'; ?> </td>
              </tr>
          
          <?php 
          endwhile;
          wp_reset_postdata();
          ?>

        </tbody>            
        
        <tfoot>
          <tr>
            <th>Invoice No.</th>
            <th>Customer Name</th>
            <th>Due Date</th>
            <th>Payment Status</th>
          </tr>
        </tfoot>

      </table>


    <?php
    else :
      esc_html_e( 'No Invoices!', 'jdsofttech' );
    endif;
    ?>
  </div>


<?php get_footer(); ?>